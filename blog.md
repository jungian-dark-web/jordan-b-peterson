# Peterson Blog Posts

Loosely organized by category. 

* [Welcome](#welcome)
* [Letters](#letters)
* [Maps of Meaning](#maps-of-meaning)
* [Psychology](#psychology)
* [Social](#social)
* [Politics](#politics)
* [Economics](#economics)
* [Philosophy](#philosophy)
* [Archetypal](#archetypal)
* [12 Rules](#12-rules)
* [Transcripts](#transcripts)
* [Biblical Series](#biblical-series)
* [Intellectual Dark Web](#intellectual-dark-web)

### Welcome

#### [Welcome to My New Webpage and Blog](https://www.jordanbpeterson.com/welcome/welcome/)

I will be posting my podcast, live events, lecture transcripts, blogs and much more on this new site. Subscribe to Dr. Peterson's Mailing List

#### [Book List](https://www.jordanbpeterson.com/books/book-list/)

Many people have written to me asking what they should read to properly educate themselves. Here is a list of books that I found particularly influential in my intellectual development. I wrote

#### [Thoughts On My Mother’s 80th Birthday](https://www.jordanbpeterson.com/family/thoughts-on-my-mothers-80th-birthday/)

This week my mother, Beverley Anne Peterson, turns 80. She was born on February 6, 1939, in Naicam, Saskatchewan, a small, attractive, thriving prairie town, back when prairie towns were communities with

#### [Announcing the Peterson Fellowship at Acton School of Business](https://www.jordanbpeterson.com/opportunities/peterson-fellowship-acton/)

I am pleased to announce today the establishment of a partnership with the Acton School of Business, under the stewardship of Acton Co-Founder and Master Teacher Jeff Sandefer. The Acton curriculum provides

### Letters
#### [Authentic Speech](https://www.jordanbpeterson.com/books/authentic-speech/)

I received a letter from a PhD student out of the University of Houston who asked me the following question: Could you please point to good literature about authentic speech? I have heard

#### [Letters: From a Mother of Teens and a Few Young Men](https://www.jordanbpeterson.com/political-correctness/letters/)

Over the last few years, I have received many tens of thousands of emails. I stopped even trying to keep up in about February or so of 2017. But I read them

#### [Kwakwaka’wakw controversy](https://www.jordanbpeterson.com/media/kwakwakawakw-controversy/)

Last week, Pankaj Mishra @nybooks offered his opinions about my life and my work in the New York Review of Books. He also touched upon my affiliation with

#### [Concerned Letter from a Self Authoring User](https://www.jordanbpeterson.com/self-authoring/selfauthoring-com-user-concerned-user/)

Selfauthoring.com is a series of online writing exercises designed to help people articulate the past, present and future. It's based in part on the work done by James Pennebaker. His book on


### Maps of Meaning

#### [Maps of Meaning: Suggested Readings & Russian Translation](https://www.jordanbpeterson.com/books/maps-of-meaning-intro/)

In 1999 Routledge published my book, Maps of Meaning: The Architecture of Belief. It was the results of more than fifteen years of work -- thousands of hours of obsessive thinking and writing.

#### [Two More Excerpts from Maps of Meaning: The Architecture of Belief](https://www.jordanbpeterson.com/books/two-more-excerpts-from-maps-of-meaning-the-architecture-of-belief/)

As I mentioned in my last blog post, Three Excerpts from Maps of Meaning, I recently recorded an audio version of Maps of Meaning: The Architecture of Belief (Routledge, 1999), now available at Audible.

#### [Three Excerpts from Maps of Meaning: The Architecture of Belief](https://www.jordanbpeterson.com/books/three-excerpts-from-maps-of-meaning-the-architecture-of-belief/)

I recently recorded an audio version of Maps of Meaning: The Architecture of Belief (Routledge, 1999), now available at Audible. I believe that the audio version will make the book much more accessible,


### Psychology 

#### [I Didn’t Say That](https://www.jordanbpeterson.com/blog-posts/i-didnt-say-that/)

Something very strange and disturbing happened to me this week. If it was just relevant to me, it wouldn’t be that important (except perhaps to me), and I wouldn’t be writing this column

#### [On the Psychological and Social Significance of Identity](https://www.jordanbpeterson.com/blog-posts/on-the-psychological-and-social-significance-of-identity/)

Back in September of 2016, I released three videos, expressing my concern about Bill C-16, which was then under consideration by the federal government, following the passage of similar legislation in a


### Social
#### [The Rules for Making Peace](https://www.jordanbpeterson.com/scientific-papers/making-peace/)

A paper I wrote in 2005: Peacemaking Among Primates - JB Peterson
Imagine two groups of people. Group A has their territory, and the rules that govern it. Group B has their

#### [On the New York Times and “Enforced Monogamy”](https://www.jordanbpeterson.com/media/on-the-new-york-times-and-enforced-monogamy/)

My motivated critics couldn't contain their joyful glee this week at discovering my hypothetical support for a Handmaid's Tale-type patriarchal social structure as (let's say) hinted at in Nellie Bowles' New York Times article



### Politics
#### [U of T Debate on Bill C-16 and Human Rights Legislation](https://www.jordanbpeterson.com/philosophy/debate-bill-c-16/)

Here is the debate held Nov 19 at 9:30 am at the University of Toronto on Free Speech, Political Correctness and Bill C-16. Participants included Dean David Cameron, who introduced the debate.

#### [An Update on PC Debate](https://www.jordanbpeterson.com/philosophy/update/)

I have been negotiating the terms of a proposed debate/discussion about political correctness with the University of Toronto administration. However, they are concerned about the legality of my recent rejection of legislatively-imposed

#### [The Great Ideological Lie of Diversity](https://www.jordanbpeterson.com/blog-posts/lie-of-diversity/)

If you are a Canadian faculty member, there is a reasonable chance that you recently received an email or letter from Statistics Canada. The Survey of Postsecondary Faculty and Researchers was designed to

#### [Equity: When the Left Goes Too Far](https://www.jordanbpeterson.com/political-correctness/equity-when-the-left-goes-too-far/)

It seems to me that the identifying factors of the radical left types that dominate the humanities and social sciences (and, increasingly, the HR departments of corporations) most particularly constitute the

#### [Bill C-16 Discussed in Senate by Hon. Donald Neil Plett](https://www.jordanbpeterson.com/political-correctness/c-16-hon-donald-neil-plett/)

Hon. Donald Neil Plett: Colleagues, last week Bill C-16, gender identity and gender expression, passed third reading in the other place without a recorded vote. This came on the heels of the

#### [Print Media Surrounding Anti-PC Dialogue](https://www.jordanbpeterson.com/political-correctness/media-surrounding-anti-pc-dialogue/)

A List of Related Print Media Coverage 

#### [Cambridge University Rescinds my Fellowship](https://www.jordanbpeterson.com/blog-posts/cambridge-university-rescinds-my-fellowship/)

From @CamDivinity, this morning (Wed, Mar 20, 2019): “Jordan Peterson requested a visiting fellowship at the Faculty of Divinity, and an initial offer has been rescinded after a further review.” I visited
Cambridge

#### [Comment on the APA Guidelines for the Treatment of Boys and Men](https://www.jordanbpeterson.com/political-correctness/comment-on-the-apa-guidelines-for-the-treatment-of-boys-and-men/)

Beware the Ideologues in Psychologists’ Clothing The American Psychological Association (APA) recently released their Guidelines for Psychological Practice with Boys and Men (paralleling, in principle, their 2007 guidelines for girls and women). It

#### [Notes on my Kavanaugh Tweet](https://www.jordanbpeterson.com/political-correctness/notes-on-my-kavanaugh-tweet/)

This week (October 5, 2018), I responded to a Twitter thread consisting of a conversation between Bret Weinstein, the American biologist and evolutionary theorist who was once (before the controversy) a professor at

#### [Durham City Council Purchases Unearned Virtue with the Currency of Denouncement](https://www.jordanbpeterson.com/political-correctness/durham-city-council-purchases-unearned-virtue-with-the-currency-of-denouncement/)

A few days ago (July 6, 2018) Mayor Pro Tempore Jillian Johnson and her colleagues on the Durham city council saw fit to release a statement on FaceBook concerning my upcoming 12 Rules

#### [The moral obligation of the moderate leftists](https://www.jordanbpeterson.com/blog-posts/the-moral-obligation-of-the-moderate-leftists/)

Video available at Big Think. I would like to talk briefly about depolarization on the Left and the Right, because I think there’s a technical problem that needs to be addressed. So here’s what


### Economics
#### [Thought Economics: A Conversation with Dr. Jordan B. Peterson](https://www.jordanbpeterson.com/blog-posts/thoughteconomics/)

Interview by Vikas S. Shah MBE on Thought Economics “The world,” writes Dr. Jordan B Peterson, “can be validly construed as a forum for action, or as a place of

### Philosophy
#### [Existentialism in Australia and New Zealand](https://www.jordanbpeterson.com/tour/existentialism-australia-new-zealand-tour/)

I have been touring in Australia and New Zealand since February 05, speaking in most of the major cities in both countries, to audiences ranging in size from 1500 to 5500. This

#### [Response to Vox “Feminist Philosopher” Dr. Kate Manne of Cornell](https://www.jordanbpeterson.com/media/response-to-vox-feminist-philosopher-dr-kate-manne-of-cornell/)

On June 6, journalist Sean Illing (email posted on Vox: sean.illing@vox.com) interviewed Assistant Professor of Philosophy (Cornell Philosophy Department) Dr. Kate Manne (the “feminist philosopher”) (Dr Kate Manne's Website) about me and my

#### [Postmodernism: definition and critique (with a few comments on its relationship with Marxism)](https://www.jordanbpeterson.com/philosophy/postmodernism-definition-and-critique-with-a-few-comments-on-its-relationship-with-marxism/)

This is from today's AMA on Reddit: http://j.mp/2s2TIEL DEFINITION AND CRITIQUE Postmodernism is essentially the claim that (1) since there are an innumerable number of ways in which the world can be interpreted and

#### [The Forward Fiasco (starring Ari Feldman, Jordan B Peterson, Deborah Lipstadt and Helen Chernikoff)](https://www.jordanbpeterson.com/philosophy/the-forward-fiasco/)

On May 4, I received the following email from my publicist, Rob Greenwald (of the agency Rogers and Cowan: www.rogersandcowan.com). I recently hired Rob to help me plan a more conscious media


### Archetypal
#### [Some thoughts on (and an offer) for the New Year](https://www.jordanbpeterson.com/political-correctness/some-thoughts-on-and-an-offer-for-the-new-year/)

A very old idea and a very new and practical application (1) Mesopotamia: Emperor and Gods The idea that the old year dies and is renewed with the new is very ancient.

#### [Prairie Requiem](https://www.jordanbpeterson.com/poetry/prairie-requiem/)

Saskatchewan In the spring the snow crystallizes and glints the sun now has some warmth black patches appear in the fields the creeks and rivers overflow their banks people say hello again to

#### [Notes on a dream in Oslo](https://www.jordanbpeterson.com/political-correctness/notes-on-a-dream-in-oslo/)

So it’s 2:39 AM in Oslo, Norway. I woke up in a too-hot hotel room out
of a fitful nightmare, which I can only partially remember. I haven’t


### 12 Rules

#### [Toronto Event: 12 Rules for Life: An Antidote to Chaos – February 5, 2018](https://www.jordanbpeterson.com/events/book-tour-toronto/)

Dr. Peterson will be speaking at the Appel Salon in Toronto, Ontario, on February 5, 2018.The precise topic has not yet been determined, but he will talk for at least part

#### [3AW693 News interview – The Burden of being the Voice of Millions](https://www.jordanbpeterson.com/media/neil-mitchell/)

with Neil Mitchell Peterson is the author of 12 Rules For Life: An Antidote to Chaos and he joined Neil Mitchell for wide-ranging discussion, including: The burden of being the

#### [12 Rules Tour: Missive from Cambridge](https://www.jordanbpeterson.com/events/12-rules-tour-missive-from-cambridge/)

The last two days in Cambridge were relentless, but in the best
possible way. Tammy (my wife, who travels with me, and carefully ensures that I know where I am going now

#### [Trouble at the University of Amsterdam](https://www.jordanbpeterson.com/political-correctness/trouble-at-the-university-of-amsterdam/)

The Dutch publisher of 12 Rules for Life made arrangements for me to speak at the University of Amsterdam on October 31 to about 300 students, hosted by the U of A's Room

#### [New Tour Dates](https://www.jordanbpeterson.com/events/new-tour-dates/)

Demand for the original 12 Rules for Life April-June tour venues and cities was high enough so that we added 24 more lectures and conversations, below. The majority of these will be


### Transcripts
#### [IQ, Politics, and the Left: A Conversation with Douglas Murray Transcript](https://www.jordanbpeterson.com/transcripts/douglas-murray/)

#### [Nina Paley: Animator Extraordinaire Transcript](https://www.jordanbpeterson.com/transcripts/nina-paley/)

#### [Aspen Ideas Festival: From the Barricades of the Culture Wars Transcript](https://www.jordanbpeterson.com/transcripts/aspen/)

#### [The Master and His Emissary: A Conversation with Dr. Iain McGilchrist Transcript](https://www.jordanbpeterson.com/transcripts/iain-mcgilchrist/)

#### [Postmodernism: History and Diagnosis Transcript](https://www.jordanbpeterson.com/transcripts/postmodernism-history-and-diagnosis/)

#### [Ideology, Logos & Belief with Transliminal Media Transcript](https://www.jordanbpeterson.com/transcripts/transliminal/)

#### [Responsibility and Meaning with Lewis Howes Transcript](https://www.jordanbpeterson.com/transcripts/lewis-howes/)

#### [Pain and Suffering with Lewis Howes Transcript](https://www.jordanbpeterson.com/transcripts/lewis-howes-2/)

#### [Truth and Responsibility with Aubrey Marcus Transcript](https://www.jordanbpeterson.com/transcripts/aubrey-marcus/)

#### [Oxford Union Full Address and Q&A Transcript](https://www.jordanbpeterson.com/transcripts/oxford-union/)

#### [Sunday Special with Ben Shapiro Transcript](https://www.jordanbpeterson.com/transcripts/ben-shapiro-1/)

#### [Freedom & Tyranny with Russell Brand Transcript](https://www.jordanbpeterson.com/transcripts/russell-brand-2/)

#### [On the Death and Resurrection: A Psychological View in Five Parts Transcript](https://www.jordanbpeterson.com/transcripts/death-and-resurrection/)

#### [Camille Paglia & Jordan Peterson – Modern Times Transcript](https://www.jordanbpeterson.com/transcripts/camille-paglia/)

#### [On the so-called “Jewish Question”](https://www.jordanbpeterson.com/psychology/on-the-so-called-jewish-question/)

The players of identity politics on the far right continue ever-so-pathologically to beat the anti-Semitic drum, pointing to the over-representation of Jews in positions of authority, competence and
influence (including revolutionary movements). I'm



### Biblical Series
#### [Biblical Series I: Introduction to the Idea of God Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-i/)

#### [Biblical Series II: Genesis 1: Chaos & Order Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-ii/)

#### [Biblical Series III: God and the Hierarchy of Authority Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-iii/)

#### [Biblical Series IV: Adam and Eve: Self-Consciousness, Evil, and Death Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-iv/)

#### [Biblical Series V: Cain and Abel: The Hostile Brothers Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-v/)

#### [Biblical Series VI: The Psychology of the Flood Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-vi/)

#### [Biblical Series VII: Walking with God: Noah and the Flood Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-vii/)

#### [Biblical Series VIII: The Phenomenology of the Divine Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-viii/)

#### [On the ark of the covenant, the cathedral, and the cross: Easter Message I](https://www.jordanbpeterson.com/philosophy/on-the-ark-of-the-covenant-the-cathedral-and-the-cross-easter-message-i/)

There has to be a bridge between the finite and the infinite. There has to be a place where the ephemeral meets the eternal. There has to be a bridge between the knowable

#### [Biblical Series IX: The Call to Abraham Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-ix/)

#### [Biblical Series X: Abraham: Father of Nations Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-x/)

#### [Biblical Series XI: Sodom and Gomorrah Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xi/)

#### [Biblical Series XII: The Great Sacrifice: Abraham and Isaac Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xii/)

#### [Biblical Series XIII: Jacob’s Ladder Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xiii/)

#### [Biblical Series XIV: Jacob: Wrestling with God Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xiv/)

#### [Biblical Series XV: Joseph and the Coat of Many Colors Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xv/)


### Intellectual Dark Web
#### [Facts and Values/Science and Religion: Notes on the Sam Harris Discussions (Part I)](https://www.jordanbpeterson.com/philosophy/facts-and-values-science-and-religion-notes-on-the-sam-harris-discussions-part-i/)

Sam Harris and I met in Vancouver on June 24 and 25 for what amounted
to five hours of intense discussion about the possibility of a universal morality with a solid foundation.

#### [Facts from Values? Not without an intermediary: Notes on the Sam Harris Discussions (Part II)](https://www.jordanbpeterson.com/philosophy/facts-from-values-not-without-an-intermediary-notes-on-the-sam-harris-discussions-part-ii/)

Sam Harris and I met in Vancouver on June 24 and 25 for what amounted
to five hours of intense discussion about the possibility of a universal morality with a solid foundation.

#### [On facts, values, rationality and stories: Part III of Response to Harris](https://www.jordanbpeterson.com/philosophy/on-facts-values-rationality-and-stories/)

Note: this is the most difficult of the three postings on Harris’s
ideas. The other two are available here (Part 1) and here (Part 2). Much of this is taken from

#### [My New Year’s Message… to the world](https://www.jordanbpeterson.com/blog-posts/new-years-message-world/)

Dear World: On January 16, I am going to talk with Sam Harris, on his
podcast, Waking Up with Sam Harris. Dr. Harris is one of the so-called
New Atheists, of which there

#### [My New Year’s Letter to the World](https://www.jordanbpeterson.com/philosophy/new-years-letter/)

Dear World: On January 16, I am going to talk with Sam Harris, on his
podcast, Waking Up with Sam Harris. Dr. Harris is one of the so-called
New Atheists, of which there

#### [Dr. Peterson on Joe Rogan Podcast](https://www.jordanbpeterson.com/political-correctness/dr-peterson-joe-rogan-podcast/)

Live on the Joe Rogan Podcast. Talking about Political Correctness, Psychology, Ideology, the human condition and many other complicated ideas.