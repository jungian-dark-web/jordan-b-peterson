# Dr. Jordan B Peterson

## Internal Links
* [Blog Index](blog.md)
* [Podcast Index](podcast/) 
  * [S2 E21: Territory, Hierarchy, Security, and Fear](podcast/JP-S2-EP-21-Territory-Hierarchy-Security-Fear_Calgary-AB.md) - Transcript (in progress)
* [Youtube Index](youtube.md) 

## Course Material
* [Psych 230h - Personality and its Transformations](personality-and-its-transformation/)
  * [Personality and its Transformations - Introduction and Overview](personality-and-its-transformation/01-course-introduction.md) - Transcription
* [Psychology 434 – Maps of Meaning](maps-of-meaning/)


## Peterson's Resources
* [Jordan B Peterson's Homepage](https://www.jordanbpeterson.com)
  * [jordanbpeterson.com/classes/](https://www.jordanbpeterson.com/classes/)
* [Jordan B Peterson on Quora](https://www.quora.com/profile/Jordan-B-Peterson)
  * [What are the most valuable things everyone should know?](https://www.quora.com/What-are-the-most-valuable-things-everyone-should-know)
* [Self Authoring](https://www.selfauthoring.com)
  > People who spend time writing carefully about themselves become happier, less anxious and depressed and physically healthier. They become more productive, persistent and engaged in life. This is because thinking about where you came from, who you are and where you are going helps you chart a simpler and more rewarding path through life.
  >
  > The Past Authoring Program helps you remember, articulate and analyze key positive and negative life experiences.
  >
  > The Present Authoring Program has two modules. The first helps you understand and rectify your personality faults. The second helps you understand and develop your personality virtues.
  >
  > The Future Authoring Program helps you envision a meaningful, healthy and productive future, three to five years down the road, and to develop a detailed, implementable plan to make that future a reality.
  >
  > Put your past to rest! Understand and improve your present personality! Design the future you want to live! The Self Authoring Suite will improve your life.
* [Understand Myself](https://www.understandmyself.com)
  > The understandmyself.com process, based on a personality scale known as the *Big Five Aspects* scale (developed by Dr. Colin DeYoung, Dr. Lena Quilty, and Dr. Jordan B Peterson in Dr. Peterson's lab) extends the Big Five description, breaking down each of the five traits into two higher-resolution aspects.


### Literature

* [Maps of Meaning](https://www.jordanbpeterson.com/maps-of-meaning/)
* [12 Rules for Life](https://www.jordanbpeterson.com/12-rules-for-life/)
* [Petersons Great Books List](https://www.jordanbpeterson.com/great-books/)
* [Profile on Research Gate](https://www.researchgate.net/profile/Jordan_Peterson2)

## Further Information
* [Jordan Peterson Resources](https://thort.space/70487003) - [Thortspace](https://www.thortspace.com) - Breakthrough in Collaborative Visual Thinking (Mindmapping)

