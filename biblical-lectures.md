# Jordan Peterson's Biblical Series

I find Peterson's treatment of Biblical stories to be the most important of Peterson's podcast lectures. I know of no-one else who has drawn such a comprehensive pychological meaning from them, able to speak so profoundly to all, cutting across barriars, regardless of ones spiritual convictions.

* [The Psychological Significance of the Biblical Stories: Genesis - Youtube Playlist](https://www.youtube.com/playlist?list=PL22J3VaeABQD_IZs7y60I3lUrrFTzkpat)


#### Introduction to the Idea of God

* [Biblical Series I: Introduction to the Idea of God - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-i/) 
  - [video - I](https://youtu.be/f-wWBGo6a2w)
  - [podcast #18](https://www.jordanbpeterson.com/podcast/episode-18/) 

> Lecture 1 in my Psychological Significance of the Biblical Stories series from May 16th at Isabel Bader Theatre in Toronto. In this lecture, I describe what I consider to be the idea of God, which is at least partly the notion of sovereignty and power,
  
#### Genesis – Chaos and Order

* [Biblical Series II: Genesis 1: Chaos & Order - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-ii/) 
  - [video - II: Genesis 1](https://youtu.be/hdrLQ7DpiWs)
  - [podcast #19](https://www.jordanbpeterson.com/podcast/episode-19/) 

> Lecture II in my Psychological Significance of the Biblical Stories from May 23 at Isabel Bader Theatre, Toronto. In this lecture, I present Genesis 1, which presents the idea that a pre-existent cognitive structure (God the Father) uses the Logos, the Christian

#### God and the Hierarchy of Authority

* [Biblical Series III: God and the Hierarchy of Authority - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-iii/)
  - [video - III](https://youtu.be/R_GPAl_q2QQ)
  - [podcast #21](https://www.jordanbpeterson.com/podcast/episode-21/)

> Lecture 3 from my Psychological Significance of the Biblical Stories Lecture Series. Although I thought I might get to Genesis II in this third lecture, and begin talking about Adam & Eve, it didn't turn out that way. There was more to be said about


#### Adam and Eve: Self-Consciousness, Evil, and Death
* [Biblical Series IV: Adam and Eve: Self-Consciousness, Evil, and Death - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-iv/) 
  - [video - IV: Adam and Eve](https://youtu.be/Ifi5KkXig3s)
  - [podcast #22](https://www.jordanbpeterson.com/podcast/episode-22/)

> Lecture 4 in my Psychological Significance of the Biblical Stories lecture series. I turned my attention in this lecture to the older of the two creation accounts in Genesis: the story of Adam and Eve. In its few short paragraphs, it covers: 1. the emergence


#### Cain and Abel: The Hostile Brothers

* [Biblical Series V: Cain and Abel: The Hostile Brothers - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-v/)
  - [video - V: Cain and Abel](https://youtu.be/44f3mxcsI50)
  - [podcast #23](https://www.jordanbpeterson.com/podcast/episode-23/)

> Bible Series V: Cain and Abel: The Hostile Brothers The account of Cain and Abel is remarkable for its unique combination of brevity and depth. In a few short sentences, it outlines two diametrically opposed modes of being -- both responses to the emergence of

#### The Psychology of the Flood

* [Biblical Series VI: The Psychology of the Flood - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-vi/) 
  - [video - VI](https://youtu.be/wNjbasba-Qw)
  - [podcast #24](https://www.jordanbpeterson.com/podcast/episode-24/)

> Lecture 6 in my Psychological Significance of the Biblical Stories lecture series. The story of Noah and the Ark is next in the Genesis sequence. This is a more elaborated tale than the initial creation account, or the story of Adam and Eve or Cain

#### Walking With God: Noah and the Flood

* [Biblical Series VII: Walking with God: Noah and the Flood - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-vii/) 
  - [video - VII: Walking with God](https://youtu.be/6gFjB9FTN58)
  - [podcast #25](https://www.jordanbpeterson.com/podcast/episode-25/)

> Lecture 7 in my Psychological Significance of the Biblical Stories series. Life at the individual and the societal level is punctuated by crisis and catastrophe. This stark truth finds its narrative representation in the widely-distributed universal motif of the flood. Mircea Eliade, the great Romanian

#### The Phenomenology of the Divine

* [Biblical Series VIII: The Phenomenology of the Divine - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-viii/) 
  - [video - VIII](https://youtu.be/UoQdp2prfmM)
  - [podcast #26](https://www.jordanbpeterson.com/podcast/episode-26/)

> Lecture 8 in my Psychological Significance of the Biblical Stories series. In the next series of stories, the Biblical patriarch Abram (later: Abraham) enters into a covenant with God. The history of Israel proper begins with these stories. Abram heeds the call to adventure, journeys

[Background to Lecture VIII: Abrahamic Stories, with Matthieu & Jonathan Pageau](https://youtu.be/f3vqXCLhJLE)
  > I had this 90 minute discussion with Jonathan Pageau, carver of Orthodox icons (http://www.pageaucarvings.com/index.html) and YouTube broadcaster (https://www.youtube.com/user/pageaujo...), as well as his brother Matthieu, who  recently finished a draft of a book on the bible. I did so as part of the background research I was doing for the 8th lecture in my series The Psychological Significance of the Biblical Stories. We talked about the nature of the narrative landscape of the Bible, focusing on the Abrahamic stories, which constitute the subject material for that 8th and other forthcoming lectures.

#### Easter Message

* [On the ark of the covenant, the cathedral, and the cross: Easter Message I](https://www.jordanbpeterson.com/philosophy/on-the-ark-of-the-covenant-the-cathedral-and-the-cross-easter-message-i/)

> There has to be a bridge between the finite and the infinite. There has to be a place where the ephemeral meets the eternal. There has to be a bridge between the knowable

#### The Call to Abraham

* [Biblical Series IX: The Call to Abraham - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-ix/) 
  - [video - IX](https://youtu.be/GmuzUZTJ0GA)
  - [podcast #27](https://www.jordanbpeterson.com/podcast/episode-27/)

> Lecture 9 in my Psychological Significance of the Biblical Stories series. In this lecture, I tell the story of Abraham, who heeds the call of God to leave what was familiar behind and to journey into unknown lands. The man portrayed in the Bible as

#### Abraham: Father of Nations

* [Biblical Series X: Abraham: Father of Nations - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-x/) 
  - [video - X: Abraham](https://youtu.be/3Y6bCqT85Pc)
  - [podcast #28](https://www.jordanbpeterson.com/podcast/episode-28/)

> Lecture 10 in my Psychological Significance of the Biblical Stories series. The Abrahamic adventures continue with this, the tenth lecture in my 12-part initial Biblical lecture series. Abraham's life is presented as a series of encapsulated narratives, punctuated by sacrifice, and the rekindling of his

#### Sodom and Gomorrah

* [Biblical Series XI: Sodom and Gomorrah - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xi/) 
  - [video - XI](https://youtu.be/SKzpj0Ev8Xs)
  - [podcast #29](https://www.jordanbpeterson.com/podcast/episode-29/)

> Lecture 11 in my Psychological Significance of the Biblical Stories series. Often interpreted as an injunction against homosexuality (particularly by those simultaneously claiming identity as Christians and opposed to that orientation), the stories of the angels who visit Abraham, bless him, and then rain destruction

#### The Great Sacrifice: Abraham and Isaac

* [Biblical Series XII: The Great Sacrifice: Abraham and Isaac - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xii/) 
 - [video - XII: The Great Sacrifice](https://youtu.be/-yUP40gwht0)
 - [podcast #30](https://www.jordanbpeterson.com/podcast/episode-30/)

> Lecture 12 in the Psychological Significance of the Biblical Stories series In this, the final lecture of the Summer 2017 12-part series The Psychological Significance of the Biblical Stories, we encounter, first, Hagar's banishment to the desert

#### Jacob’s Ladder – Part 1

* [Biblical Series XIII: Jacob’s Ladder - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xiii/) 
  - [video - XIII](https://youtu.be/A9JtQN_GoVI)
  - [podcast #33](https://www.jordanbpeterson.com/podcast/episode-33/)

> The Psychological Significance of the Biblical Stories starts up after a two month hiatus with the first half of the story of Jacob, the founder of Israel ("those who wrestle with God"), the man who robs his brother of his birthright, is deceived


#### Jacob: Wrestling with God

* [Biblical Series XIV: Jacob: Wrestling with God - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xiv/)
  - [video - XIV: Jacob](https://youtu.be/DRJKwDfDbco)
  - [podcast #35](https://www.jordanbpeterson.com/podcast/episode-35/)

> In this lecture, I present the second half of the story of Jacob, later Israel (he who struggles with God). After serving his time with his uncle Laban, and being deceived by him in the most karmic of manners, Jacob returns to his

#### Joseph and the Coat of Many Colors

* [Biblical Series XV: Joseph and the Coat of Many Colors - Transcript](https://www.jordanbpeterson.com/transcripts/biblical-series-xv/) 
  - [video - XV](https://youtu.be/B7V8eZ1BLiI)
  - [podcast #36](https://www.jordanbpeterson.com/podcast/episode-36/)

> Lecture 14 in my Psychological Significance of the Biblical Stories lecture series. This lecture closes 2017, and the book of Genesis. In it, I present the story of Joseph who, as the wearer of the coat of many colors, is profoundly adaptable, courageous, adaptable, merciful and

#### Death and Resurrection of Christ

* [The Death and Resurrection of Christ: A Commentary in Five Parts](https://youtu.be/xPIanlF6IwM)
  > This video is derived from five sources:
  >
  > Part 1: The Nature of Experience (from my first book Maps of Meaning: The Architecture of Belief: https://www.jordanbpeterson.com/maps-of-meaning/)
  > 
  > Part 2: Some Axioms of the Christian Revolutionary Story (created for this video)
  > 
  > Part 3: Narratives and Sacrifice (taken from Rule 7: Pursue what is meaningful, not what is expedient, in my new book, 12 Rules for Life (https://www.jordanbpeterson.com/12-rules-for-life/)
  > 
  > Part 4: On the Ark of the Covenant, the Cathedral and the Cross: Easter Message I (from my blog at https://jordanbpeterson.com/blog/) 
  > 
  > Part 5: The Psychological Meaning of the Death and Resurrection of Christ: Easter Message II (an extended version of an Easter article I wrote for the London Sunday Times (https://bit.ly/2GEnC7B)
  > 
  > The idea of the death and resurrection has a psychological meaning, in addition to its metaphysical and religious significance. It can be thought of as part of the structure of narrative that sits at the basis of our culture. It includes elements of sacrifice (associated with delay of gratification and the discovery of the future) and psychological transformation (as movement forward in life often requires the death of something old and the birth of something new). 
  > 
  > This five-part commentary is an attempt to explain such ideas in detail so that they can be understood, as well as “believed.”  