# Psychology 434 – Maps of Meaning

* [jordanbpeterson.com/classes/psychology-434](https://www.jordanbpeterson.com/classes/psychology-434/)
  > This course is based on the book Maps of Meaning: The Architecture of Belief. Maps of Meaning lays bare the grammar of mythology, and describes the relevance of that grammar for interpretation of narrative and religion, comprehension of ideological identification, and understanding of the role that individual choice plays in the maintenance, transformation and destiny of social systems.
* [jordanbpeterson.com/maps-of-meaning](https://www.jordanbpeterson.com/maps-of-meaning/)

### TOC

* [Course Readings](#course-readings)
* [2017 Youtube Playlist](#2017-youtube-playlist)
* [Blog Posts](#blog-posts)
* [Podcast Episodes](#podcast-episodes)
* [2016 Youtube Playlist](#2016-youtube-playlist)
* [2015 Youtube Playlist](#2015-youtube-playlist)
* [Farsi Youtube Playlist - طرح واره‌های معنا](#farsi-youtube-playlist-%D8%B7%D8%B1%D8%AD-%D9%88%D8%A7%D8%B1%D9%87%D9%87%D8%A7%DB%8C-%D9%85%D8%B9%D9%86%D8%A7)

### Course Readings

[Maps of Meaning: The Architecture of Belief](https://www.jordanbpeterson.com/maps-of-meaning/)
  * [Initial Chapters](https://www.jordanbpeterson.com/docs/434/Initial_Chapters.pdf)

**Action and its Imitation (January 12)**
  * Rizzolatti, G., Fogassi, L. & Gallese, V. (2001). [Neurophysiological mechanisms underlying the understanding and imitation of action.](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/Rizzolatti%20G%20Neurophysiology%20of%20imitation%20Nat%20Rev%20Neuro%202001.pdf) Nature Reviews, 2, 661-670.

**Memory, Novelty and Anxiety (January 26)**
  * Vinogradova, O.S. (2001). [Hippocampus as comparator.](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/vinogradova.pdf) Hippocampus, 11, 578-598.
  * Ohman, A. & Mineka, S. (2003). [The malicious serpent: snakes as a prototypical stimulus for an evolved module of fear. Current Directions in Psychological Science](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/Ohman%20A%20Malicious%20Serpent%20Current%20Directions%202003.pdf), 12, 5-9.
  * Peterson, J.B. (2013). [Three kinds of meaning and the management of complexity](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/Peterson%20JB%20Three%20Kinds%20of%20Meaning%20Final%203.pdf). In In K. Markman, T. Proulx, & M. Linberg (Eds.). The Psychology of Meaning. Washington, DC: American Psychological Association.

**Dominance and Hierarchy (February 02)**
  * Kravitz, E.A. (2000). [Serotonin and aggression: insights gained from a lobster model system and speculations on the role of amine neurons in a complex behavior](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/kravitz.pdf). Journal of Comparative Physiology, 186, 221-238.

**Higher Order Control (Feb 23)**
  * Miller, E.K. (2001). [The prefrontal cortex](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/Miller%20EK%20Prefrontal%20cortex%20Nature%20Neuro%202000.pdf). Nature Neuroscience Reviews 1, 59-65.

**Vision (Mar 02)**
  * Isbell, L. (2006). [Snakes as agents of evolutionary change in primate brains](https://www.jordanbpeterson.com/docs/434/Assigned_Papers/Isbell%20L%20(2006)%20Snakes%20as%20agents%20of%20evolutionary%20change%20in%20primate%20brains%20JHE.pdf). Journal of Human Evolution, 51, 1-35.

### [2017 Youtube Playlist](https://www.youtube.com/playlist?list=PL22J3VaeABQAT-0aSPq-OKOpQlHyR4k5h)

1. [2017 Maps of Meaning 01: Context and Background](https://youtu.be/I8Xc2_FtpHI) (2:31:27)
2. [2017 Maps of Meaning 02: Marionettes & Individuals (Part 1)](https://youtu.be/EN2lyN7rM4E) (2:23:34)
3. [2017 Maps of Meaning 03: Marionettes and Individuals (Part 2)](https://youtu.be/Us979jCjHu8) (2:26:56)
4. [2017 Maps of Meaning 04: Marionettes and Individuals (Part 3)](https://youtu.be/bV16NEWld8Q) (2:12:27)
5. [2017 Maps of Meaning 05: Story and Metastory (Part 1)](https://youtu.be/RudKmwzDpNY) (2:22:29)
6. [2017 Maps of Meaning 06: Story and Metastory (Part 2)](https://youtu.be/nsZ8XqHPjI4) (2:27:27)
7. [2017 Maps of Meaning 07: Images of Story & Metastory](https://youtu.be/F3n5qtj89QE) (2:11:51)
8. [2017 Maps of Meaning 08: Neuropsychology of Symbolic Representation](https://youtu.be/Nb5cBkbQpGY) (2:21:22)
9. [2017 Maps of Meaning 09: Patterns of Symbolic Representation](https://youtu.be/yXZSeiAl4PI) (2:16:50)
10. [2017 Maps of Meaning 10: Genesis and the Buddha](https://youtu.be/7XtEZvLo-Sc) (2:18:45)
11. [2017 Maps of Meaning 11: The Flood and the Tower](https://youtu.be/T4fjSrVCDvA) (2:32:24)
12. [2017 Maps of Meaning 12: Final: The Divinity of the Individual](https://youtu.be/6V1eMvGGcXQ) (2:25:56)

### Blog Posts

* [Maps of Meaning: Suggested Readings & Russian Translation](https://www.jordanbpeterson.com/books/maps-of-meaning-intro/)
  > In 1999 Routledge published my book, Maps of Meaning: The Architecture of Belief. It was the results of more than fifteen years of work -- thousands of hours of obsessive thinking and writing.
* [Two More Excerpts from Maps of Meaning: The Architecture of Belief](https://www.jordanbpeterson.com/books/two-more-excerpts-from-maps-of-meaning-the-architecture-of-belief/)
  > As I mentioned in my last blog post, Three Excerpts from Maps of Meaning, I recently recorded an audio version of Maps of Meaning: The Architecture of Belief (Routledge, 1999), now available at Audible.
* [Three Excerpts from Maps of Meaning: The Architecture of Belief](https://www.jordanbpeterson.com/books/three-excerpts-from-maps-of-meaning-the-architecture-of-belief/)
  > I recently recorded an audio version of Maps of Meaning: The Architecture of Belief (Routledge, 1999), now available at Audible. I believe that the audio version will make the book much more accessible,

### Podcast Episodes

* [#9 – Monsters of Our Own Making](https://www.jordanbpeterson.com/podcast/episode-9/) - February 27th, 2017
  > * Part 1: Maps of Meaning: 1 Monsters of Our Own Making.
  >   - This is the first of a 13-part 30 minute episode television series broadcast by TVO presenting Dr. Jordan B Peterson’s lectures on his book, Maps of Meaning: The Architecture of Belief. The lecture provides a good introduction to the psychology of mythology and religion, based on the idea that stories from these domains describe the world as a place of action, rather than, as science does, a place of things.
  > * Part 2: Maps of Meaning: 2 Contending with Chaos
  > * Part 3: Maps of Meaning: 3 Becoming Like Gods
* [#10 – Games People Must Play](https://www.jordanbpeterson.com/podcast/episode-10/) - March 6th, 2017
  > * Maps of Meaning 4: Games People Must Play – Starting at 0:32
  > * Maps of Meaning 5: Grappling with Fear – Starting at 27:59
  > * Maps of Meaning 6: Submitting to Order – Starting at 55:23
* [#12 – Contemplating Genesis](https://www.jordanbpeterson.com/podcast/episode-12/) - March 20th, 2017
  > * Maps of Meaning 7: Contemplating Genesis – Starting at 0:32
  > * Maps of Meaning 8: Dwelling on Paradise – Starting at 27:57
  > * Maps of Meaning 9: Becoming A Self – Starting at 55:22
* [#13 – Maps of Meaning 10 – 13](https://www.jordanbpeterson.com/podcast/episode-13/) - March 26th, 2017
  > * Maps of Meaning 10: Figuring Evil – Starting at 0:32
  > * Maps of Meaning 11: Losing Religion – Starting at 27:57
  > * Maps of Meaning 12: Truths that Matter – Starting at 55:22
  > * Maps of Meaning 13: The Force Within – Starting at 1:22:45

### [2016 Youtube Playlist](https://www.youtube.com/playlist?list=PL22J3VaeABQAGbKJNDrRa6GNL0iL4KoOj)

1. [2016 Lecture 01 Maps of Meaning: Introduction and Overview](https://youtu.be/bjnvtRgpg6g) (1:40:55)
2. [2016 Lecture 02 Maps of Meaning: Playable and non-playable games](https://youtu.be/RcmWssTLFv0) (1:10:54)
3. [2016 Lecture 03 Maps of Meaning: Part I: The basic story and its transformations](https://youtu.be/ux6TVYqdN-E) (1:30:28)
4. [2016 Lecture 03 Maps of Meaning: Part II: The basic story -- and its transformations](https://youtu.be/DmpUQEDRIKA) (30:32)
5. [2016 Lecture 04 Maps of Meaning: Anomaly](https://youtu.be/DjYqkPrCvXQ) (1:30:33)
6. [2016 Lecture 05: Maps of Meaning: Part I: Anomaly and the brain](https://youtu.be/ZHmklvx9oJ4) (1:39:24)
7. [2016 Lecture 05 Maps of Meaning: Part II: The brain, continued](https://youtu.be/cFS6fPLQ024) (46:04)
8. [2016 Lecture 06 Maps of Meaning: Part I: The primordial narrative](https://youtu.be/mJI0hVV-5Vs) (1:37:57)
9. [2016 Lecture 06 Maps of Meaning: Part II: The Primordial Narrative continued](https://youtu.be/5Q_GIHDpuZw) (35:32)
10. [2016 Lecture 07 Maps of Meaning: Part I: Osiris, Set, Isis and Horus](https://youtu.be/HueFqvz1oDU) (1:36:36)
11. [2016 Lecture 07 Maps of Meaning: Part II: Osiris, Set, Isis and Horus](https://youtu.be/sta4zLcTAII) (27:53)
12. [2016 Lecture 08 Maps of Meaning: Part I: Hierarchies and chaos](https://youtu.be/PcYLzW1B6cY) (1:28:24)
13. [2016 Lecture 09 Maps of Meaning: Genesis](https://youtu.be/Gacjj2aCo7Q) (45:38)
14. [2016 Lecture 10 Maps of Meaning: Gautama Buddha, Adam and Eve](https://youtu.be/F7T5cg1a77A) (2:47:27)

### [2015 Youtube Playlist](https://www.youtube.com/playlist?list=PL22J3VaeABQByVcW4lXQ46glULC-ekhOp)

1. [2015 Maps of Meaning Lecture 01a: Introduction (Part 1)](https://youtu.be/4tQOlQRp3gQ) (1:56:44)
2. [2015 Maps of Meaning Lecture 1: Introduction (Part 2)](https://youtu.be/rM8JsibkrI8) (29:21)
3. [2015 Maps of Meaning Lecture 02a: Object and Meaning (Part 1)](https://youtu.be/mO9LUWs5M60) (1:37:07)
4. [2015 Maps of Meaning Lecture 02b: Object and Meaning (Part 2)](https://youtu.be/6Rd10PQVsGs) (42:13)
5. [2015 Maps of Meaning Lecture 03a: Narrative, Neuropsychology & Mythology I (Part 1)](https://youtu.be/6NVY5KdSfQI) (1:03:03)
6. [2015 Maps of Meaning Lecture 03b: Narrative, Neuropsychology & Mythology I (Part 2)](https://youtu.be/3nAIAPYuD7c) (1:32:25)
7. [2015 Maps of Meaning 04a: Narrative, Neuropsychology & Mythology II / Part 1 (Jordan Peterson)](https://youtu.be/rlGqUfIgJfc) (51:17)
8. [2015 Maps of Meaning 04b: Narrative, Neuropsychology & Mythology II / Part 2 (Jordan Peterson)](https://youtu.be/YCc-Rk1GPpQ) (1:04:33)
9. [2015 Maps of Meaning 05b: Narrative, Neuropsychology & Mythology III / Part 1 (Jordan Peterson)](https://youtu.be/Ov5pYNPi358) (2:09:06)
10. [2015 Maps of Meaning 05b: Mythology: Enuma Elish / Part 2 (Jordan Peterson)](https://youtu.be/VJVMtUb-LEY) (20:20)
11. [2015 Maps of Meaning 06a: Mythology: Introduction / Part 1 (Jordan Peterson)](https://youtu.be/r_ShAseOvNE) (56:50)
12. [2015 Maps of Meaning 06b: Mythology: Egyptian Myths / Part 2 (Jordan Peterson)](https://youtu.be/aI-pET9YD6A) (52:49)
13. [2015 Maps of Meaning 07a: Mythology: Chaos / Part 1 (Jordan Peterson)](https://youtu.be/44dcUoh0oT4) (1:49:02)
14. [2015 Maps of Meaning 07b: Mythology: Chaos / Part 2 (Jordan Peterson)](https://youtu.be/rnw4SXX7cGY) (29:25)
15. [2015 Maps of Meaning 08a: Mythology: The Great Mother / Part 1 (Jordan Peterson)](https://youtu.be/NOzjfqO6-K8) (2:04:59)
16. [2015 Maps of Meaning 08b: Mythology: The Great Mother / Part 2 (Jordan Peterson)](https://youtu.be/w1scgquS2mo) (19:52)
17. [2015 Maps of Meaning 09a: Mythology: The Great Father / Part 1 (Jordan Peterson)](https://youtu.be/134BCxbMUlU) (1:08:58)
18. [2015 Maps of Meaning 09b: Mythology: The Great Father / Part 2 (Jordan Peterson)](https://youtu.be/tIZb0YEcyNo) (51:09)
19. [2015 Maps of Meaning 10: Culture & Anomaly / Part 1 (Jordan Peterson)](https://youtu.be/Bj6HgQBNiZE) (1:33:36)
20. [2015 Maps of Meaning 10: Genesis I / Part 2 (Jordan Peterson)](https://youtu.be/sJVtAIIHxu0) (58:12)
21. [2015 Maps of Meaning 11: Genesis II / Part 1 (Jordan Peterson)](https://youtu.be/Q_2UYIuvDXI) (1:55:19)
22. [2015 Maps of Meaning 11: Conclusion - The Hero / Part 2 (Jordan Peterson)](https://youtu.be/G7U9el_yVhI) (42:55)



### [Farsi Youtube Playlist - طرح واره‌های معنا](https://www.youtube.com/playlist?list=PL22J3VaeABQCDeqQjrNLGqdlEbVR9Lowk)

1. [‫طرح واره‌های معنا ۱/۱۳:‌هیولا های مخلوق انسان (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/z-AmpAhspCg) (28:51)
2. [‫طرح واره های معنا ۲/۱۳:‌ رویارویی با آشوب (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/1YfSQdcmlMA) (28:37)
3. [‫طرح واره های معنا ۳/۱۳:‌ ستیز با آشوب (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/1FosxUP-24A) (28:27)
4. [‫طرح واره های معنا ۴/۱۳:‌ باز هایی که باید بکنیم (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/A9r5fhXOldY) (28:03)
5. [‫طرح واره های معنا ۵/۱۳ : دست و پنجه نرم کردن با ترس (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/8VRN61my3_k) (28:32)
6. [‫طرح واره های معنا ۶/۱۳ :‌تسلیم در برابر نظم (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/vy49tdc7Ee8) (28:04)
7. [‫طرح واره های معنا ۷/۱۳ :‌تاملاتی در باب سفر پیدایش (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/UvuBHqJP8_s) (28:30)
8. [‫طرح واره های معنا ۸/۱۳: تفکر درباره بهشت (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/UvtPyO02SIQ) (29:11)
9. [‫طرح واره های معنا ۹/۱۳ : فرد شدن (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/HzK1vFY2Fms) (28:30)
10. [‫طرح واره های معنا ۱۰/۱۳ :‌فهم شر (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/NHpnifSUvtQ) (28:16)
11. [‫طرح واره های معنا ۱۱/۱۳: از دست دادن دین (با زیر نویس فارسی و انگلیسی)‬‎](https://youtu.be/l9yp45hA2Fk) (29:01)
12. [‫طرح واره های معنا ۱۲/۱۳ :‌ حقیقت های مهم (با زیرنویس فارسی و انگلیسی)‬‎](https://youtu.be/jbfKOJdez5Q) (29:07)
13. [‫طرح واره های معنا ۱۳/۱۳ : قدرت درون (با زیرنویس فارسی و انگلیسی)‬‎](https://youtu.be/tHqC4tJztPI) (29:09)
