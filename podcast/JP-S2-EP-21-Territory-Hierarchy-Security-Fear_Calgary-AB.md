# S2 E21: Territory, Hierarchy, Security, and Fear
#### Jordan B. Peterson’s “12 Rules for Life” lecture from Calgary, AB

This is the first episode I ever listened to from Peterson, in it he expressed some ideas that I needed to hear, and think the world does, too.


## Transcript
0:06  
Thank you. Thank you. Much appreciated.

1:39  
I see that 600 activists didn't scare you off, eh? Kind of shocked by that, you know, that was the that was the longest petition that anybody's managed to put together so far. it's impressive. It was good of the theater not to lose their spine. You need a spine you know? cuz you can't stand up straight without one.

### Seek Enemies so They Can Inform You

2:14  
I've been working on some ideas that I'm going to share with you tonight. Recently, I was at four venues with Sam Harris, two in Vancouver and two in one in Dublin and one in London. Those will be released relatively soon as soon as we get the production straightened out and the audio cleaned up and all of that. They they helped me think some things through. I figured out some things that I hadn't got clear in 25 years of thinking. That's why it's useful to talk to somebody that you disagree with, if you can have an actual conversation with them especially if they're sharp, because they push you to make things clearer than they were before.

3:00  
And you might say, well, who cares if they're clearer... but, you know, you act out your thoughts. And so if your thoughts aren't clear, then your actions aren't precise, and you get into trouble. I mean, it's kind of self evident, right? That's why you think, and you should think sharply, because you need to act carefully. And so it's very useful to talk to somebody who has thought things through who doesn't think the same way you do. Because like, what the hell do you know? And if you talk to somebody that disagrees, there's real some real possibility they'll tell you some things you don't know. That's rule nine, right? Assuming that the person that you're talking to knows something you don't, because you need to know what you don't know. Unless everything's perfect for you. That seems highly unlikely. So well, that's the that's the mark of your ignorance, right?

4:00  
If your life isn't everything, that it should be, then you don't know enough. Simple as that. If you talk to people who think exactly the same way you do, then all they're going to do is tell you the things you already know. And clearly, if your life isn't everything it could be, then what you know, isn't enough. So you should seek out some enemies and see if they can inform you. Because then while they'll be a bit more to you, and maybe you'll think a bit more clearly, and you won't have such a wretched and miserable time of it.

### Pursuit of Meaning in the Cold War

4:32  
30 years ago, when I started thinking about all the things that I wrote about in maps of meaning in my first book, which I released, by the way, in audio form on June 12. If you thought the *12 Rules for Life* was useful, and want to go into the ideas that are in it more deeply, then that's a good way of doing it. It's a very complicated book, but it was out of that book that *12 Rules for Life* arose. And the reason that I wrote both of these books, fundamentally it started with, with something I was obsessed about.

5:10  
I got obsessed about the fact of the Cold War back in the late 70s and early 80s. And it seemed like.. it's surreal to me, that the world had divided itself up, obviously, not just to me, but that the world had divided that up itself up into two armed camps, each of which pledged allegiance, let's say, to a different system of beliefs. That's not so unbelievable. But what was unbelievable that was that those beliefs were so important to the east and the west. That we seemed willing to put the whole world to the torch, or at least risk that. to protect those systems.

6:01  
At the height of the Cold War, each side, the Soviet side and the Western Capitalist side, let's say, had 10s of thousands of hydrogen bombs aimed at each other on a hair trigger. I don't know how much about hydrogen bombs, probably not a lot, hopefully not a lot. But, you know, that in Hiroshima and Nagasaki that Americans dropped out and bombs on those cities to devastating effect. A hydrogen bomb has an atom bomb for the trigger.

6:38  
That's the difference between a fissiom bomb and a fusion bomb. For a fusion bomb. The bomb is just what gets it started. And we had 10s of thousands of those things aimed at each other. There's still plenty of them around. It's not like we're out of the woods, out of the weeds, I guess. But, you know, it's better than it was.

### Belief Systems More Important than *Everything*?

7:00  
I was really obsessed with trying to understand how could it be? How could it be that belief systems could be so important that they that their maintenance was worth sacrificing everything to, you know, not just our own lives say but the past and, and the future life itself? Maybe it's it struck me as a very interesting psychological issue like how could it be that belief systems in some sense could be more important than everything? It just didn't make sense to me.

I had been studying political science and I kind of liked that in my first year or two when I studied political science. I studied the great political thinkers, political philosophers, you know, the classic political philosophers, Plato and Aristotle and Hobbes and Rousseau and John Stuart Mill and John Locke, etc. And I really liked that I thought that was extremely helpful.

8:00  
Then when I got up into the upper years, in my undergraduate degree, I was taught, essentially that people were motivated by economic concerns, and that was fundamentally what drove conflict and war. And I thought, No, it's not a very good explanation because it's missing something to say that people fight over what they value is to say almost nothing at all. Obviously, people fight over what they value. So it's not helpful. The question is, why do they value what they value? That's the fundamental question. And so I stopped studying political science.

I started studying psychology because it struck me that the fundamental problem wasn't political or economic or sociological, it was psychological. I believe that's the case, which is why *12 Rules for Life* and *Maps of Meaning* concentrate so much on the individual, because that's the psychological level of analysis. I think for this particular problem, why our belief systems are so important, it's the psychological level of analysis.

### Defense Against Meaninglessness

9:01  
So I had this intuition that you need a belief system to structure your action in the world.

You need a belief system to get along with other people. And you need a belief system to protect you from meaninglessness, and meaninglessness, that's nihilism and depression... and the whole end of what would you call it, the existential spectrum that you can inhabit, where you're essentially just flooded with negative emotion with emotional pain and anxiety, that's unbearable.

That's sort of the default condition of human beings I would say it's probably the default condition of life itself to very well widely distributed, religious idea that life is essentially suffering. It stands to reason because people and other creatures are vulnerable.

10:00  
We can be hurt, we can be killed, and, and things tend to go from bad to worse of their own accord. And so the default condition of life isn't satisfaction or happiness, or even neutral, emotional being, it's suffering. And you need something to set against that suffering, partly because you actually have to do something about it right? You have to act in the world, you have to act productively in the world, or you don't just feel terrible, you actually degenerate and die. The suffering is reflective of your actual vulnerability. So it's a practical necessity to act in the world. It's also a psychological necessity act in the world. And you have to organize your actions towards some end. That should be a valuable end, it's your belief systems that organize your actions towards some end, and your belief system protects you from being overwhelmed with negative emotion.

### Conflict of Belief

11:02  
The problems is, this group of people might have belief system a and this group of people might have belief system B. And when you bring them into contact with each other, there's conflict, and conflict is the way of the world. I mean, when you have a relationship with someone individually, you both have slightly different belief systems or maybe substantially different belief systems and you try to inhabit the same space within the confines of an intimate relationship, there's going to be conflict. And it's because you need your belief system. And yet, Person A and Person B think about the world differently. So conflict is inevitable.

11:41  
Conflict between individuals, conflict between groups, conflict between nations. The problem is is that we're so technologically powerful now. We can't afford conflict between nations.

11:56  
People aren't going to just give up their belief systems because if you give up your belief system, then you don't have anywhere to go, and you don't have anything to do, and you fall into... nihilistic suffering, and that's no good, because it's too painful. Well, it's counterproductive, first of all... plus, it's painful. Plus, it's anxiety provoking, but it's worse than that... this is where things really go off the rails. If you don't have a belief system that's working for you, then not only are you in emotional pain, not only are you not doing well, and in emotional pain, and anxious, but that isn't where the degeneration stops.

### Resentment and Beyond

12:41  
If you take anyone, including yourself, and make them suffer stupidly, pointlessly, they don't stop with being in emotional pain and being anxious. They move from that to being resentful and revengeful and cruel, and murderous, and past that there's plenty of places past murderous, genocidal. There's places past genocidal, because it's one thing to want to wipe people out. And it's another thing to want them to be as miserable as possible before you wipe them out. And that's where things end... although I suspect there and explored nooks and crannies of hell that even extend beyond that.

If you're familiar, to some degree, with human history, or perhaps particularly the history of the 20th century, you know, that people can go places that are so unimaginably unimaginably dark, that you can barely tolerate reading about them, right, much less, you know, being in a situation where the thing you're reading about is actually happening.

13:48  
Meaninglessness is not a place that anyone wants to go. You don't have your belief system, but if you have your belief system, then you're going to have conflict with everyone that has a different belief system, and we can't afford unlimited conflict anymore.

14:10  
That was a very shocking set of discoveries for me. I thought that if you thought something through, then the answer would appear merely as a consequence of having done the thinking properly, that you wouldn't end up in a dead end where option A was terrible, and option B was terrible. And that's all the options there were.

### Another Option

14:34  
That's not the only option. There's not the only two options, you would think that it would be because the one option is you have beliefs and other options that you don't have them. doesn't seem to be any other options, But there are.

14:55  
Now I want to tell you that's a that's a way of laying out the structure of the world. On one end of reality, there's a state of nihilistic chaos. And on the other end, there's a state of totalitarian certainty. And both of those are unacceptable options. Okay, so then what's the alternative to that?

15:20  
I'm going to tell you a little bit about the way the Taoists look at the world to begin with, because they produced a conceptual system that I think is unbelievably helpful. I think it's correct.

15:36  
It's hard to understand because it's a way of portraying reality that it isn't like the way that we portray reality, as Westerners... materialist, atheists... essentially, in our ontology, in our theory of being. Now you know, we may be religious or not. We're mixture, but fundamentally, the West is a materialist and atheistic society and it's become extraordinarily technologically powerful as a consequence of that. So you can't dispute the power of that perspective.

As a set of tools, materialism is unbelievably useful, but it leaves us bereft of meaning, which is a big problem given where the lack of meaning takes people.

### Chaos and Order

16:23  
The Taoists think that reality is best construed of as a combination of yin and yang. You all know that symbol, I presume. It's circle with something that looks like a white paisley inscribed inside of it, opposite of black Paisley. Now those aren't Paisley's there are serpents there are snakes. So for the Taoists the world is a white snake and a black snake serpents described inside a circle and the in the in the white serpent's head is a black circle, small black circle. And then the black serpents head, a small white circle. The reason for that is because Yin and Yang can turn into one another at the drop of a hat.

17:15  
Yin is often represented as feminine. That's why, for example, in 12 Rules for Life, I mentioned that it's a common proclivity for human beings to represent chaos as feminine. No, I didn't invent that. The Taoists invented that, and they didn't even invent it. It's just, it's just one of the ways that symbolic proclivity manifests itself, can't be laid at my feet. I don't want to take credit for it. It's just how it is. There's reasons for it and the reasons are deep and they took me a long time to understand. there's many reasons why that relationship exists.

18:01  
We'll go through some of them. Yin is chaos, feminine, and Yang is order masculine. Now, you know, the idea that the West is a patriarchy, which is a masculine construct, that's order. That's a reflection of the same symbolic intuition. Now, I don't like the idea that the west is a patriarchy, even though it's true, in part. It's always important to remember that many things are true, in part, not just true, but true in part.

18:42  
The unnamed, unquestioned assumption on the part of the feminist types, the radical leftist types, the same sort of people who've been criticizing me, for example, for noticing that order is symbolicly masculine, are perfectly happy to proclaim that the West is a patriarchy, which is exactly the same claim.

19:17  
Then you think, if the West is a patriarchy if the West disorder and it's pathological order, and that's a patriarchy, that's masculine, then there's something that's feminine, insofar as there's something that's feminine. And it isn't the order, because we already wrapped that up in the whole patriarchal order thing. So it's got to be whatever that isn't.

19:40  
And the opposite of order is chaos. Now, you might think that chaos is a bad thing. And too much of it is, but so is too much order. So the fact that too much of something can be a bad thing doesn't mean that the thing itself is bad. You know, people poison themselves quite often, actually, by drinking too much water, but that doesn't mean water is bad. It just means that you can poison yourself if you drink too much of it. So don't do that, by the way, because I bet you didn't know you could do that. But you can.

20:17  
The Taoists believe that reality is made out of chaos and order. Yin and Yang

20:27  
And so, what's chaos? Alright, I'll tell you a bunch of tell you a bunch of things that chaos and order are. You can only get a sense of it by seeing it from different angles. That's that's really the right way of conceptualizing.

### Explored and Unexplored Territory

20:47  
So chaos and order are unexplored territory, unexplored territory. That's a good way of thinking about it, to biological way of thinking about it. So animals have unexplored territory, and they're very protective of that. Animals in zoos for example, everybody thinks, well, the animals don't like to be in the zoo. For most animals, if you open the door to the cage, they stay in the cage. That's home. And you know, if you have a cat, some of you have had cats and you've taken your cat to a new house, the cats don't like that. The cats pretty happy. And it tells because it's going around the house, and it's sniffed out every nook and cranny in that house. And it knows that there isn't anything there that eats cats.

21:29  
It's quite happy to be in that house. But then you take the cat to a new house, and that cat isn't happy. If it's a neurotic cat and go under the couch, and it'll yell for a day or two before it comes out. Because it thinks there's probably things there are things that eat cats, and there's probably some of them in this house because that's the right default assumption. Right? If you're small and edible, the right default assumption is that wherever you go, that's new. Something that eats you is there.

22:00  
So cats like explored territory, and when you put them in unexplored territory, then they get terrified. And that's just exactly what human beings are like too. Part of the reason that we don't often go places that we don't understand physically. Most of you don't attend biker bars, despite your reputation. If you do, more power to you, as far as I'm concerned, the world needs bikers.

22:35  
Most of you don't go there because that's unexplored territory. That's just a good place to get into trouble. And so you just don't go there. By the same token, you tend not to go places that are outside of your sphere of conceptual familiarity for exactly the same reason. You don't like to go places that are too strange, especially accidentally, you know, like, if you go there on purpose and, and it's part of an exploratory endeavor, then that can be an adventure, but to be dropped there. No, that's not good.

### Fear: the Default Condition of Being

One of the things that I've come to think about, as a psychologist, is the default human condition. So imagine this, you know, maybe you think you're sort of a calm person. You're not. You're just, you're just mostly in very safe places. Okay? That's a very big difference. So the default human being. So imagine that you were woken up at three o'clock in the morning, and you were stripped naked, and you're taken in the helicopter, and you're just dropped into the jungle at night, naked. Okay, that's what you're actually like, all right. That's life.

23:38  
That's life. And the fact that you're not like that all the time, means that you're somewhere that's so safe, it's absolutely beyond comprehension. And our being in this hall is a really good example of that. It's so safe here. You know, despite what the protesters were claiming, it's very safe here. There's 3000 people here, something like that. And we don't know each other... I know some, a little bit. But fundamentally, it's a whole room full of strangers. But we all believe the same thing, enough, so that we can all sit here and do the same thing, enough, so that none of us have to be in that state that we would be in if we were stripped naked and dropped into the jungle.

The reason that is the case is because we all share a belief system and we're all acting it out at the same time. Notice everybody's facing the front in this theater. Right? And nobody nobody is acting, far as I can tell, radically. If anyone was you'd be very uncomfortable about it. So every single one of us in here is imitating everyone else. And the reason we're doing that is to keep the existential terror at bay.

24:51  
Really, really.

### Teaching Rats to Be Afraid

24:56  
I'll give you an example. I'll give you an example. This is a way that Psychologists made a mistake. They made lots of mistakes, but they've learned a lot, but they've made lots of mistakes. Behavioral psychologists thought that fear was learned. Okay, so here's here's how they figured that out. Mostly studying animals and animals are quite a lot like people. Or at least... psychologists study rats, and you think they're not much like people. A rat is more like a person than your idea of a person is like a person. Rats make perfectly... even though they're not perfect.

A rats a complicated thing. You try building one at your kitchen, it's very hard to build a rat. It's very complicated, and makes a pretty good model for a person. So you have a lab rat, he's in his cage, which makes some kind of a strange thing to begin with. Because rats don't live solitary lives in cages. They live fully engaged social lives in rat hierarchies. A lab rat is kind of a weird thing. It's like, it'd be like you would be if you were in solitary confinement. "there's a person in a box".. People don't live in boxes. So you can't even be a person in a box really. So you could live, you could exist. But you're not a person in a box. You're a pretty, pretty peculiar thing. Be that as it may.

26:13  
You got your rat. He's in his cage. And this is his home cage, he knows it. So you watch that rat and think that's a normal rat. And then you want to train the rat to be afraid. The behavioral idea was that rats could feel pain, and that they could learn what signals pain and what they would manifest to the signal of pain would be fear or anxiety. That's how it was learned. So what you do to scare a rat... go to a box, put the rat in. You can electrify the bottom of the cage a little bit, give them a shock. So not too bad a shock, but then you turn the light on just before the shock happens. Do that like 50 times and then soon as the light goes on, the rat goes like this, because the rat learns the light signifies a shock. Then the rat manifests anxiety or fear to the light. So it's learned to be afraid, and that's the theory. Got your calm rat. You teach him how to be afraid. The real rat is the calm rat and you teach them how to be afraid. But that's actually not the way rats work. It's backwards.

### You Don't Learn Fear

27:21  
If you take a rat by the tail, and you move them to a new cage, you don't have to electrify the ship, the floor for him to be afraid. As soon as you put the rat in the cage that he hasn't been in. This is the rat. He's frozen. He's he doesn't move. And that's because rats know that predators can see things that move, especially horizontally. It's why a cat's eyes are slit, by the way, because it can detect horizontal movement better because most of the things that cats eat, like rats, move horizontally. So rats that don't die of being eaten by cats learn that. If you don't know where the hell you are, then you should just do this.

28:00  
Right, frozen in terror. Okay, so then that's the rat. That's the rat man when he's when he doesn't know anything. When a rat doesn't know anything, that's the real rat, the rat hasn't learned anything, and he doesn't risk moving. That's life.

28:19  
You don't have to learn fear. You learn the opposite of fear, you learn security, you don't learn fear. And you learn security by incorporating a belief system that you share with everyone else. If your society is working well, then you have the belief system, and so does everyone else, and the belief system then matches what everyone else does. And so you don't have to be terrified out of your skull.

### Trying not to Scare Ourselves and Eachother

28:48  
And so that's why everybody acts the same, roughly speaking. That's why you even act the same as you acted yesterday or the day before. You actually don't want to scare yourself too badly.

I'm dead serious about this man, dead serious. Take a naive person. Give them a little military training, put them out on the battlefield. Let them come in contact with some of the things that lurk inside of them.

29:17  
They go out on the battlefield, and they do some things they didn't think they could do. They get Post Traumatic Stress Disorder, right? Because they learned that they have no idea who they are. And they have no idea what they can do. And they learn that they were acting like they always acted partly so they didn't terrify themselves out of their skulls. And that's the human condition. And so part of the reason that we're conventional and that we adopt routines and that we mirror those routines with everyone else's. Not only do we not want other people to scare us too badly. We don't want ourselves to scare us too badly. And it's a real issue.

30:00  
People develop post traumatic disorder, soldiers in particular, but people in general, develop post traumatic stress disorder, not because something terrible happened to them. That's not enough. Because lots of terrible things are going to happen to all of you. And most of you aren't going to develop Post Traumatic Stress Disorder. from it says mere tragedy that will do it. malevolence, does it. Sometimes it's because you encountered someone malevolent, and maybe you were a little naively unprepared for it, and so that just blows you into bits... or you encounter it within yourself.

30:33  
Back to the rat. He's frozen, in apprehension of what might look beyond. That's the chaos right now. He's in chaos that rat. It's got no order whatsoever. And as the consequences gripped and frozen in place, and then what is he do? He can't move because if he moved he might get eaten.

30:59  
Like a little kid. When they're around six months old, four months old, they don't have any fear. They don't develop fear till they can start to move. That's when the that's when the anxiety circuits kick in. Well, what good is it being afraid if you can't move. Anyways, it's not helpful. Right? Once you can move, you can not move.

### Telling Children There aren't Monsters in the Dark

31:23  
And so you need the not move circuit to be activated. You know, there's a famous, Far Side cartoon, monster snorkel, this little kid under covers on his bed. It's like a turtle. He's completely underneath there and he's got a, like a pipe going out so he can breathe. Of course, there's monsters everywhere in the dark around him. Monster snorkel. It's like yeah, that's that's the world of childhood and that's because the child in his bed in the dark, is the same creature as the rat in the new cage. And the child's imagination populates the dark with Monsters. And you might say to your child there's no monsters in the dark. But you know, that's stupid.

32:11  
Right? You all know why, right? Because no kidding, there's plenty of monsters in the dark. Now it might be the case that right then and there because of the walls that you've put up, you notice there's walls in your house, right? They keep the roof on, and they keep the monsters out. That's why you have walls and then, of course, there's there's walls around everything. There's walls around the state. That's the Army, keep the monsters out. And so the child there was perfectly well that the dark is full of monsters. And that none of them happened to be in his room at that moment. But that's called comfort for the child because their their ancestral wisdom populates their imagination with the monsters of the dark. You know, and so you don't actually tell your child that there are no monsters in the dark.

33:00  
What you might do is get them out of bed and have them look under the bed and have them look in the closet and look and explore around and to see for themselves that everything's okay but more importantly, to see for themselves that they're the sort of creature who go out in the dark, and see if the monsters are there and prevail, because you cannot teach your child that life is safe, because it's not. But you can teach them that they're brave enough to prevail, which is an entirely different thing. Yes, there are monsters in the dark, but you can handle them. That's way different. It's way different. And if you're a psycho therapist with a shred of credibility, you never, it doesn't work anyways, if you have people who come to you with agoraphobic or obsessive compulsive disorder, or post traumatic stress disorder, telling them that they're exaggerating the danger is of zero utility. It doesn't work a bit.

34:00  
What you do is you help them learn to confront the things they're afraid of voluntarily, or disgusted by another variation of negative emotion more relevant to obsessive compulsive disorder. You don't teach them that the world is safe, you teach them that they're a lot tougher than they think. And that's a way better lesson because the world is not safe, and you are way tougher than you think. So that's a very, very good thing. That's the optimism that sort of at the bottom of all this pessimism.

34:27  
... so the rat frozen. Who knows what the rat is imagining? Rats are intrinsically afraid of cat odor. Even if a rat has never ever seen a cat or smelled one. If you put a fan behind the cat and blow the cat oder towards the rat, it'll freeze doesn't matter if it's ever seen a rat. We're like that with snakes By the way, we're actually we're actually biologically wired to be afraid of snakes. So that's only been established I would say in about the last 10-15 years.

So we have this capacity to populate the darkness with the monsters that have been our enemies for God only knows how long millions of years. And that's what happens in a child's imagination when he or she is afraid of the dark.

### Frozen in Terror

35:18  
So the rat is standing there frozen, and nothing happens. And that's a form of exploration, right? Because if you're frozen in terror and nothing happens, you're noticing. And so what you're learning is that was long as I stand here, frozen and terror, nothing is eating me. And so that's actually a form of safety, right? Because you're not being eaten at that moment.

35:44  
Evidently, the place that you are now that you don't know anything about, it's the sort of place where if you stand frozen and terror, nothing tears you to shreds, and that's actually good news. So you can start to relax a little bit, not much, a little bit.

36:00  
So the rat starts to relax and the first thing he does is sniff. And rats use their their olfactory system far more than human beings do. Most animals. brains are organized around smell ours are organized around vision, but that's not the case for most animals. So the rattle start to sniff, then if you can't smell anything signifies danger, then he'll sniff more new start to move his head. And then he starts to sort of thought he'll make little movements and eventually start to explore his whole cage. And then when he explores this whole cage, and nothing eats him, and nothing scares him, then he'll calm down.

36:35  
Then a behavioral psychologist can come along and teach them how to be afraid. But the but the thing is, it's really important that the psychologist got it backwards. Because safety, calmness, security is not the default condition. It's learned. When you become afraid what you do is unlearn it, and you can unlearn all of it. If you unlearn all of it, you end up agoraphobic or you have severe obsessive compulsive disorder, something like that. You can unlearn all of it. Trauma will do that to you. Sometimes a physical illness will do that big enough shock will do that. There's lots of things that will do it.

### Hierarchies

37:21  
So the rat is in chaos, and then he builds himself a little structure of order. And then he's calm, but the chaos is still there, because it never goes away. Now I said already. Rats don't live in isolation. They live in complex social groups. I made some reference to this in *12 Rules for Life* and the first rule: "stand up straight with your shoulders back" when I was talking about hierarchies. And I was talking about hierarchies for a reason, It wasn't to prove they were inevitable.

38:00  
That chapter is being criticized a lot by the people who've gone after me. In the press, let's say Peterson is justifying the patriarchy or justifying hierarchy. It's like, number one, noticing something exists is not the same as justifying it. Okay, so that's important. Now, so I noticed that hierarchies exist. Now you wouldn't think that'd be too much of a problem since it's also the proclivity of the left wing radicals that have gone after me to also notice that hierarchies exist. In fact, that's all they do. They just spent all their time noticing that hierarchies exist. Now, they have a problem.

38:51  
Okay, so they have a problem. They think that hierarchies oppress and dispossess. That's true. They do.

39:03  
One of the reasons that I wrote the first chapter was because let's say you are a compassionate person, and you're also a good person. Those are not the same thing. Just because you feel sorry for someone does not mean you're good. Good is way harder than just feeling sorry for someone. So if you're running around patting yourself on the back, because you feel sorry for people, you're like, 1% of the way to being a good person. That's it.

39:37  
It's partly because it's partly because feeling sorry for people can actually be really damaging to them. Like if you feel too sorry for people, if you treat them like they're too fragile and over protect them, you do them a terrible disservice. That's especially true if they happen to be your children. So not only are you not a good person, if you're just because you're compassionate, you might be a bad person because you're too compassionate. That happens a lot.

40:00  
I noticed in my clinical practice, for example, no client ever came and told me, I need to see a psychologist because my parents made me too independent. That never happened. Right? It never happened. But the reverse happened a lot. No, I can't get away from the stifling mill, you have my family. Right, that happened a lot. And it's crushing. So.

### Hierarchies are a Bigger Problem Than You Think

40:27  
Alright, so back to hierarchies. Rats live in hierarchies. It's an important observation that rats live in hierarchies, because the radical leftists, for example, who are upset about the fact that hierarchies exist and dispossessed people, and that they produce unequal distribution of resources. They have a point, but they're not very serious about what they're doing. That's the thing. That's what bothers me because it actually is a big problem that hierarchies dispossess and oppressed people.

It's a really big problem. But it's a way bigger problem than the people who are using that argument to speak for the dispossessed are willing to admit. You take Karl Marx, for example, "please", to use an old joke. He believed that you could place the existence of hierarchies that dispossess and oppress at the feet of the West and capitalism.

41:35  
You can't. Unquestionably, that's wrong. And it's wrong in an extraordinarily dangerous way. Because even if you're concerned about the dispossessed, because you're a compassionate and you're good. If you think that hierarchical structures exist because of the West and capitalism, you're not going to solve the problem that you set out to solve because

42:00  
hierarchies are way worse problem than the mere West and capitalism. Otherwise animals wouldn't live in hierarchies. chimps live in hierarchies. Wolves live in hierarchies. Dogs live in hierarchies. Rats live in hierarchies. Birds live in hierarchies. lobsters live in hierarchies. Right. Okay. So why did I pick lobsters? I'm going to be haunted by lobsters for the rest of my life. 

Why did I pick lobsters? Well, for a couple of reasons, first of all, we diverged from our common ancestor with crustaceans 350 million years ago. So the fact that they live in hierarchies, like we do, means that hierarchies have been around for 350 million years, that's older than trees. It's older than flowers.

43:00  
Hierarchies are a permanent structure of reality, that's order. That's the Yang part of the gang. It's permanent. And it's so permanent. And this is another reason that I use lobsters is that your nervous system is not only adapted to hierarchies, it's adapted to hierarchies as if there's hardly anything else that's more real. So here's an example.

So your emotional stability is dependent, at least in part, on the efficiency with which the lower parts of your brain, the ancient parts of your brain, produce the neural chemical serotonin. And if you produce enough serotonin, then your negative emotions stay pretty nicely under control. You're also not too impulsive on the positive emotion front. More importantly, you're not too much emotional pain, you're not too much disgusted, and you're not terrified out of your skull. It's actually really important to you, that your serotonin levels stay high. It's more important than anything else.

Here's the rub. Part of the way your brain determines how much serotonin to produce is by looking at where you fit in the hierarchy. And the higher you are up in the hierarchy, the more serotonin you produce. The reason for that is that your brain assumes that if you're high up in the hierarchy, you have lots of friends and you have a pretty comfortable place to live. And you have lots of opportunities in front of you. And you have resources to draw on if things go to hell. And so it's okay that you're not terrified out of your skull.

If you're at the bottom of the hierarchy, and you don't have any skills, and you don't have any employment, and you don't have any friends, and everything's uncertain as hell, then your serotonin levels plummet, and you're overcome by anxiety and terror. And you think well, and it's worse than that. Because if you're overcome by anxiety and terror, and you don't get a lot of positive emotion out of that, by the way, if you're overcome by anxiety and terror, you produce a tremendous amount of the stress hormone, hormone cortisol, and then you die faster, because cortisol makes you age more quickly.

The reason it does that essentially is like if you're in an emergency, which you are, if you're at the bottom of the hierarchy, because you're like one slip from disaster when you live there all the time, then your brain assumes that you better bloody well be prepared for whatever the hell is going to happen next. And so it cranks up the cortisol levels, and that puts everything that you are on edge for emergency action. And that can be exhilarating in small doses. Like that's why people go on a roller coaster but as a way of life.

45:23  
It makes you old. It steals resources from your future and burns them up into present. And so high levels of cortisol increase your probability of cancer and heart disease and diabetes and alzheimer's and obesity and like you name it. You just have to suppresses your immune system, because who cares if you die of the flu in two weeks if there's a tiger chasing around a tree right now? That's exactly it.

45:53  
So, not only do hierarchies exist, you can't attribute them to the vagaries of some socio economic structure like the west and capitalism. And they're so old that your brain is adapted to them. Like they're a fundamental element of reality. And so the Marxist critique, the best way to critique the Marxist critique isn't from the right wing. You just say yeah, you're right. hierarchies. oppress and dispossess, but you're wrong about the fact that they're a secondary consequence of capitalism. They're not. Period, you're wrong. 

46:32  
If you want to solve the problem of dispossession, instead of just acting like you're one of the people who want to solve the problems of dispossession, which is an entirely different thing, then you might want to get a hell of a lot more serious about the problem. And that's part of the reason that I wrote, the first chapter was to make the claim, it's like, no, this is a way worse problem than you think.

### Corrupt hierarchies are based on power, functional hierarchies are based on competence.

Now, I made a secondary claim, which is also an anti Marxist claim, because the identity politics. Marxist types also have this other idea, which is there's a hierarchy, it's a consequence of the pathological structure of the West. And it's basically a tyranny, and the way you move up it is by exercising power. Because all hierarchies are based on power. It's like, No, they're not. That's also wrong.

A corrupt hierarchy is based on power, but a functional hierarchy is based on competence. And you know, that. Take the hierarchy of plumbers, for example. So most of us presume that it's useful to have plumbers, we don't think of roving bands of plumbers, oppressing us with the necessity for plumbing. And we don't think that the best plumber is the one who's beat the most other plumbers to death with a lead pipe.

47:54  
Right. We assume that the best plumber is someone who stopped sewage from leaking in your house at a low cost, and he was pretty decent at managing his time or her time and business affairs, right? And that's how you hire a plumber. You don't have plumbers come to your door and threaten you with what will happen if you don't hire them.

Well, it's funny. But it's not funny because we have this pervasive propaganda in our culture that fundamentally what we've created is something like a tyranny. But if you break it down into its elements, it's the absurdity of that just manifests itself right away. The tyranny of emergency room nurses, there's another one of people who work in palliative care or massage therapists. There's another terrible power hierarchy, elementary school teachers. Even lawyers. Sorry, I know. Some of you are lawyers, and that's okay.

48:56  
It isn't the most tyrannical lawyers that rise to the top. It's actually the lawyers who are best at generating business for their law firm. It's the lawyers who are competent as lawyers, but who can also work effectively as entrepreneurial agents, and who bring business to their law firms, and who feed the lawyers in the law firms who are competent but can't generate business. Those are the people that rise to the top. If they're a bit psychopathic, because another alternative route to the top is to be a bit of a psychopath, and to use power. It's a very ineffective route, you have to keep moving around because people don't people figure you out fairly soon. And so you have to keep moving around and finding new suckers because if you stay in one place and you're a psychopath, people figure out figured out pretty soon they put a box around you and they just stay the hell away from you.

So the idea of a that what we live in is a tyrannical patriarchy is something that has the vaguest ghosts of truth. It's being worshiped as an unalterable axiom. The hierarchies we have can tend towards corruption, and corruption by power. That's partly why we have elections. You know, if someone's in power for like 15 years, I think the reason democracy works, in part is to rotate between right and left. Even more importantly, is just to throw people out. I'm dead serious about this. It's like, it takes a while to get a hierarchy so entrenched that it can start to become corrupt.

Maybe that takes four years, maybe takes eight years, because those are kind of the terms we allow people, right, four years, eight years, something like that in power. It's like, okay, you had your eight years, throw you out. Throw this pack of idiots out, bringing a new pack of idiots. No improvement.

### Relationship between belief systems and heirarchies.
50:54  
So back to the hierarchy. There's no getting away from hierarchies. Okay, so why? Well, there's a bunch of reasons. I told you that I'd been talking to Sam Harris about this, and about belief systems. So now I want to talk about the relationship between belief systems, and hierarchies. Because I actually don't think they're any different. I think they're the same thing. So, here's why. This is also relevant to the chaos and order issue.

51:24  
There's a lot of the world out there. There's a lot of the world and there are a lot of facts about the world. There's more of the world than you can see. And there's more of the world than you could even hypothetically see. So, and there are more facts about the world than you could ever possibly process. So things are complex beyond imagining, and everyone knows them. And that's chaos. Now, it's useful because the fact that things are complex beyond imagining is also the same thing as saying there is potential beyond imagining.

52:00  
The downside of the complexity is it's just too much. But the upside is, hey, there's more out there than about and you can mine that, and you do you do that when you go develop skills, you go develop your skills, you get more educated, or you set yourself a new challenge, right? You're out there mining potential, you're out there in the complexity, mining potential. That's why, by the way that you find gold if you go find the dragon, exactly the same idea. The dragon is this complex symbol of complexity that lurks in the darkness, the same thing that will devour the rat, but there's possibility out there. So if you go out there, you get the possibility. So that's, that's the chaotic domain.

52:39  
Because there's so much out there. And there's so many things to attend to. It's very hard to figure out what to do. And that's actually what we have to do as living creatures. We can't just see things. We have to do things about them. And here's the rub. You can only do one thing at time.

This is one of the very, very strange things about consciousness. You know, if you try to write, and watch a video at the same time, you're not doing that. What you're doing is you're writing a bit and then you're watching a bit, then you're writing a bit, then you're watching a bit, you only have one motor output system. It's kind of strange, actually, that's the case, your consciousness is an unbelievably narrow channel, people have estimated its bandwidth at four bits, right, which is like a really bad modem from 1950, when there weren't even modems. the bandwidth of your consciousness is unbelievably narrow. And what the reason for that seems to be that you have to take all of everything that there is, and you have to turn it into the one thing that you're doing. And you do that in action, because you can only do one thing at a time, but you also do it in perception. Because you can actually only look at one thing at a time or hear one thing at a time.

It's a little more complicated than that but not much. compared to all the things you could be seeing and hearing, you're hardly seeing and hearing anything. And there's lots of experiments that have shown this. So for example, change blindness experiments that show the way our visual... you kind of have a sense that you see a broad visual landscape, but you don't you're laser beaming it constantly by moving your eyes around, and then you build a picture out of these little laser beam points and your eyes are just moving around like mad, partly voluntarily, and partly involuntarily. Pinpoints of light makeup, the visual image, and you can actually see this if you pay attention to it.

For example, if I look at you, I can see your face and your eyes, but two ladies beside you, I can tell they're women. I think you're a man and I think you're a woman. And I can't tell and out here, I know your people, but you're basically just vague blurs. I know your people because I already looked there. But otherwise, thank you.

55:00  
You people over there, you just all mashed together and you're basically black and white, even though I can't tell that. So my, my central vision is very, very focused and detailed, but the peripheral vision is hardly there at all. And the way you overcome that is by looking around all the time. And so, so here's the complex problem that you have to solve that you're solving every second of your life. This is what use your belief systems for is you're using your belief system to take everything that's in the world, that chaos, that chaotic potential, and to turn it to one thing, so you have to think about that so hard. You have to filter out everything except one thing. Here's something you might be interested in. I spent a lot of time investigating psycho neuro psychopharmacology, the way the brain responds to chemicals, essentially. And when I was very interested in drug and alcohol abuse, I did my PhD thesis on alcoholism, studied a lot about the way that various drugs of abuse work.

56:00  
Hallucinogens, one of the things hallucinogens do is stop you from reducing everything to one thing. That's why people have this sense of being flooded by, say infinite potential or sometimes hell, but infinite potential. The chemicals that make up hallucinogens, the classical hallucinogens interfere with serotonergic function. That's one of the things they do. And they stop this gating from occurring. And so they open as Huxley pointed out, they open the doors of perception, they let you see far more of what's there than you would normally proceed at quite a cost, right? The cost of potentially being overwhelmed and also the cost of not being able to act. Well that's happening. And so that can be a wonderful mystical experience. But you could be eaten by a lion, when you're not able to organize running away. So it's actually very practically useful in to reduce things to one thing.

57:00  
Then the question is you've got all those things to reduce to one thing, How the hell do you do it? Because it's really, really complicated. Because there's all that material to take care of. This is what I really wanted to concentrate on tonight, because I'm just starting to get this right is you do it both collectively and neurologically at the same time. So, think of, I don't know how many of you have ever started a business. But if you start a business and you produce a new product, one of the most difficult decisions that you have to make is how much to charge for your product. Now, obviously, you have to charge more than it costs to make it. So that's, that's where you start, but you can't tell after that. It's like is it a premium product? Should you charge 100 times would it cost to make it or maybe 1000 times because maybe people only want to buy it because it's a luxury item? Or should you make it as cheap as possible, so that many, many people buy it, you know, but you don't want to make it too cheap because if you make it too cheap, then people will think it's cheap, and then they won't buy it.

58:00  
It's really hard to make a pricing decision. Because what's something? What is something worth? The answer is that question really is, what's this thing worth compared to everything else that's worth something?

58:12  
And there's a lot of other things that are worth something. There's an almost infinite number of them. That's actually why the free market works and why it's a necessity is because the only way you can decide what everything is worth is by letting your letting everyone vote on it. So you have all that brainpower that distributed brainpower, it's like externalized computation, you say let's just let everybody compete about what things are worth. And we'll, we'll watch where it settles, because otherwise we'll have to compute that ourselves. And we can't, the central Soviet in its heyday had to make 10,000 pricing decisions a day. They couldn't even calculate what a nail is worth. Like, what's a nail worth? Well, if you have to nail something together, it's worth quite a lot.

58:56  
Right? A nail can be worth a lot. So what's a nail worth? Well, there's no answer to that. Because the question is, isn't what a nail is worth? The question is, what is the nail worth in comparison to everything else that's worth something? Good luck computing that. You can't. It's technically not possible. That's partly why we have a stock markets like what's the stock worth? Well, it's whatever anybody will pay for, given that they could also buy any other stock. And how do we compute that? We let 10s of millions of people vote and even that sometimes goes completely astray. Because sometimes the stock market you know, it has a bubble and then it crashes. And it's just an indication of how difficult it is to price everything.

And you think why is he rambling on about pricing? It's a subset of the problem of valuing. To value things is to put them in a hierarchy, right to in to order them in terms of their worth. And you have to do that in order to be able to see because you can't see anything unless you focus on one thing and not on everything else.

1:00:00  
In order to value things, you have to put them in a hierarchy. In order to put them in a hierarchy, you have to let everyone participate, because otherwise you're not smart enough to put them in a hierarchy. And so that's partly what we're doing in our culture is that we're engaged in this competitive and cooperative enterprise that helps us collectively determine what everything is comparatively worth. And you think that why is that so useful? Well, it simplifies things. What should you pay attention to? Not everything. You know, that a day, you're trying to pay attention, everything, you just don't get anything done. If you pay attention to everything, you don't get anything done. So what you have to do is you have to prioritize and by prioritizing, you're reducing everything to zero except the one thing you're concentrating on. And that's insanely complicated. you wouldn't have to have a brain if that wasn't so hard, and you have a hell of a brain.

1:01:00  
You know, it is the most complicated thing in the cosmos, by orders of magnitude. It's massively complicated. It's so full of connections, that, that it's a, the number of patterns of connections in your brain far supersedes any cosmological number, you are super complicated. And the reason for that is it's really hard to take everything there is and turn it into the one thing that you should do. And then, while you think the question is how do you answer that question, what should you do? And the answer is you partly want to do what others, you partly want to do what other people think, is worthwhile. Do you won't have something to trade, if you're doing something that no one else thinks is worthwhile? Well, first of all, you're probably going to die because the probability that it's useful if no one else thinks it's useful, it's like zero. Well, not completely zero. Sometimes you're a stellar genius, but hardly ever. Most of the time you're just deluded. But now and then you're a stellar genius. So that's problem.

1:02:00  
But in any case, you should pay attention to what everyone else values. Because if you pay attention to what everyone else values and you master it, then you have something to trade with everyone else, and they value you. And so that's a good deal. And so, so this is how it works. And we have a hierarchical organization in our society. It consists of people cooperating and competing in the attempt to produce value. And you need to produce value because we need valuable things. Otherwise, we die. And we need to produce value because we need to specify what it is that we need to do. And so we have hierarchies that do the specification. And you incorporate that hierarchy. That's the perceptual structure that mediates between you and the world. So because what you end up doing is paying attention to the things that are of the value and the the question, what is the value is something that's that's decided upon collectively.

1:03:02  
Your perceptual systems are dependent on the integrity of the hierarchy. And then your psychological stability is dependent on the match between the hierarchy that you've internalized, and the hierarchy that exists outside. So if you have an internalized set of values, hierarchical structure, and you're acting on that, and it's the same structure that everyone else is acting on, then the valuable things that you pursue will be valued by other people, then you can live with other people. And so part of the reason that we need to support our belief systems back to that original problem is because they actually do protect us from chaos. Your belief system isn't just your belief system. Your belief system is the system that you use to simplify the world so that you can act in such a manner that it exists in correspondence with the way everyone else is doing that simultaneously, at least within your cultural sphere.

So part of the reason you're also happy about supporting your own cultural value system is because you have a value system, and so does your culture and they match. And so if your cultural value system gets flipped upside down, then your perceptual value system no longer works, then you can't function, then the chaos of the world comes in and overwhelms you, then you're terrified out of your skull, and you die. And so it's not surprising that people value their belief systems.

1:04:27  
That's all extraordinarily useful to know, as far as I'm concerned.

1:04:40  
There's a couple of problems here. One problem we're not going to be able to address to any great degree tonight is the fact that despite the fact that you have to have the hierarchy, because you have to have the value system, because it has to specify, so that you can act, and so that you can act with other people. There's no getting away from that. It's absolutely necessary. But hierarchies do dispossessed people.

1:05:04  
Because what happens I wrote about this and *12 Rules for Life* too, is that once you set up a hierarchy, like the number of good plumbers is far outweighed by the number of terrible plumbers, the number of good lawyers is far outweighed by the number of terrible lawyers. And it's true in every single domain. And it's viciously true. It's governed by a law prices law. Prices law is a terrible law, but it looks like a law. it governs the size of cities, it governs the size of stars, it governs the height of trees in the in the rain forest, it's another one of these laws that isn't specific to human endeavor. That which gets bigger stars to create more mass because they have more gravitational pull, so they just get bigger and bigger. So and bigger cities are more attractive because they produce more opportunity so they get bigger, and taller trees get more light so they get taller and taller.

1:06:01  
Skillful people get more and more opportunities, and they get more and more skillful. That's what happens. Rich people, because they have money, have more and more opportunities to make more money. So they make more money, you know, the 1%, you hear about that all the time. You're all in the 1%. By the way, by world standards, you're in the 1%. by historical standards, you're probably in the one 1,000th of 1%. So that's a good thing to know, if you're feeling poor.

1:06:30  
You're not, you're not that poor. So, so you know, you hear a lot about the 1%. But what you don't hear is that it's an iron law of hierarchy. And doesn't matter what the hierarchy is, it produces exactly that distribution. Hardly anyone ends up with everything. 12 richest people in the world have as much money as the bottom two and a half billion think, Oh, my God, that's terrible. It's a terrible cataclysmic consequence of the capitalist system. It's not. And one of the things that's really interesting about capitalism in the West is that every system produces unbelievable inequality and a lot of misery and, and grief. The West has figured out how to produce inequality, and some wealth.

1:07:17  
Right? So the inequality, that's a catastrophe, good luck sorting out. The wealth. That's a bloody miracle. That's absolutely unbelievable, because the rule for all of human history was plenty of inequality, no shortage of starvation and misery, and a little bit of luxury for the tiny set of people who are at the top. And for the last hundred and 50 years, not much more than that. The bottom has been being lifted up at an absolutely unbelievable rate. So we produce inequality, but thank God we produce wealth at the same time and we're kicking the hell out of absolute poverty.

1:08:01  
The number number of people in absolute poverty in the world, by the UN definition of absolute poverty, fell by half between 2000 and 2012. That one of the UN Millennium goals was to have the number of people in absolute poverty before by the year 2015. We did that three years early. Right. It was the fastest growth in wealth in human history by a huge margin. And the same thing is happening on an unbelievably immense scale. Child mortality rates in Africa are now the same as they were in Europe in 1950. It's absolutely beyond belief, plummeting maternal death rates and child birth everywhere around the world, plummeting family size, we're going to peak out at about nine and a half billion, we can probably handle that and then there's going to be a precipitous decline in human population. That's well projected out into the next 75 years. There are more forests in the Northern Hemisphere than there were 100 years ago.

1:09:00  
People are getting access to fresh water and plumbing and electricity and unbelievably potent computational devices at a rate that is absolutely beyond comprehension. I don't think you could speed it up if you tried. The fastest growing economies in the world are in Sub Saharan Africa. This system that we've put together, which is based at least on part in on the production of competent hierarchies, is producing an increment in in decrement in absolute poverty at a rate that's absolutely staggering. And so, and people don't know it, and most of this has been happening in the last 20 years. So it's something to be extraordinarily grateful for, and it's something as well, for us to wake up about, you know, we need to be smarter about these things. It is still the case that hierarchical structures dispossess because of prices law. So here's prices law, it's a terrible law.

1:09:53  
The square root of the number of people who are engaged in an activity do half the work. So if you have 10 artists, three of them paint half the paintings. But if you have 100 artists, 10 of them paint half the paintings. And if there's 1000 of them, then 30 of them paint half the paintings. And so as the domain of product, as the domain, as the size of the group engaged in the productive activity increases, the proportion of people who are doing the bulk of the productive labor decreases.

And as I said, this law seems to apply across categories, not even not just human categories, reproductive categories, for that matter. So it's the rule, rather, it's the rule. And so and that's a real problem. And so I'll close with this. I think part of the reason that we have a left wing and right wing in our political discussion is because the right wing is always saying, rah, rah for the hierarchy. And fair enough, man, especially if the hierarchy is based on competence because those hierarchies are hard to come by. And they stabilize our societies and they allow for productivity, and they allow you to perceive the world and they allow you to act and they allow you to exist in the absence of abject terror, that's a lot.

But the price we pay for hierarchies is the dispossession of the majority of people. Now, we solve that in some ways by producing a lot of different hierarchies, you know, so if you're not good at one thing, there's something else you can be good at. But there are people who just don't manage it across the entire set of hierarchies. And that's pretty rough. And the proper voice of the left is to speak on behalf of the people who are unfairly dispossessed by the hierarchy, but not at the cost of carrying the whole damn thing down. Because of a lack of understanding, first of all the depth of problem but but also, as a consequence of sharing gratitude, it would be very foolish thing for us to tear down the machinery that we built, that is accomplishing all the great things that it is accomplishing, because it produces inequality, because we don't know how to make productive machines that don't produce inequality. No one knows how to do that.

Now, that doesn't mean we don't have a responsibility to do something to mitigate the inequality, you know, and maybe that's a personal responsibility issue. You know, if you happen to be fortunate, let's say, skillful, productive and lucky, because luck plays into it, you know, then maybe you have a moral obligation to do what you can to try to make the world a better place. Which is something that I tried to stress in *12 Rules for Life*, also trying to make the case. And I'll close with this two cases.

1:12:30  
First case, the best way to move up a competency hierarchy, or more importantly, imagine there's a set of competency hierarchies. And what you want to do is you want to figure out how you could act so that the probability that you would be successful across that whole set of hierarchies is maximized. That's the issue of ethics. That's the way out of moral relativism, the ethic that's emerged, I would say, that's well articulated in Western philosophy, Western theology for that matter.

1:13:00  
The idea that there's a mode of ethical being that maximizes the probability that you will be successful in the most successful way across the set of all possible hierarchies of competence. And that's actually associated with what we would classically think about as being a good person.

1:13:19  
The most relevant part is probably being a responsible person. And one of the things that I've made a case for in *12 Rules for Life*, for example, is that, and this goes back to the beginning of the lecture, because life is characterized by vulnerability and mortality, suffering, all of that. You need meaning to offset that, has to have practical utility, because you don't want to starve to death and as a psychological utility, so that you can, you can bear your existence despite its fragility, and the most effective way of developing both of those appears to be to take responsibility.

It's this face saying, it's the same facing thing again. It's like life is characterized by suffering. What do you do about that? You face it as if you could, and you take responsibility for it as if you could. And in that responsibility, you find the meaning that allows you to deal with the suffering, practically speaking, because you get better at it, but also to triumph over because you've got something worth doing.

1:14:27  
And that's responsibility. That's also the message that goes throughout the book, but is he exemplified particularly in chapter one, because the reason that I suggested to people is that they stand up straight with your shoulders back isn't to adopt a position of tyrannical patriarchal power, but to orient themselves physically so that you confront the world and its complexity and its suffering as courageously as possible, which is what you do when you open yourself up to the world. It's not a defensive posture. It's not defeated.

1:15:00  
It's a part of your voluntary confrontation or posture of moral courage, or posture of responsibility. When there isn't a better way of dealing with things, there isn't a better way of pushing back the suffering and malevolence of the world. And there isn't a better way of being successful in functional hierarchies. And so, from the knowledge of the hierarchy and its existence, you can understand perception, you can understand the role of perception in determining action, you can understand the role of perception and action in regulating emotions, and you can derive an ethic. And all of that, as far as I'm concerned is on is what founded on on a firm foundation on a rock, not on sand. It's a reflection of the reality of the world. So thank you very much.


