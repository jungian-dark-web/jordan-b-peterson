# Jordan B Peterson Podcast -Index

The existing podcast's have been organized into some rough semblance of both chronological and catgorical order. This is mostly for convenience and congruence with my inner sensibilities.

## Directory

* [Reality and the Sacred](#reality-and-the-sacred)
* [Maps of Meaning](#maps-of-meaning---tvo)
* [God and Religion](#god-and-religion)
* [Biblical Lectures](#biblical-lectures)
* [Philosophy](#philosophy)
* [Culture](#culture)
* [Interviews](#interviews)
* [Intellectual Dark Web](#intellectual-dark-web)
* [Self Authoring](#self-authoring)
* [12 Rules for Life](#12-rules-for-life)
* [Q/A](#q-a)

## Reality and the Sacred

#### [#1 – Reality and the Sacred](https://www.jordanbpeterson.com/podcast/episode-1/)

December 7th, 2016

In a public lecture recorded by TVO, Dr Peterson describes the way the world is portrayed in deep stories, such as myths and religious representations. The world in such stories is a place of action, not a place of things, and it has its archetypal

#### [#4 – Religion, Myth, Science, Truth](https://www.jordanbpeterson.com/podcast/episode-4/)

December 30th, 2016

> It was originally recorded by Transliminal in November 2015.
> 
> * 0:00 Intro: Peterson’s Journey
> * 9:45 Religion: Crowd-control, Naive Science, or Neither?
> * 14:44 Scientists Misreading Religion
> * 17:13 Enter: Dominance Hierarchies
> * 18:14 Hierarchies, Religious Thinking, and Subjectivity
> * 19:57 Determining the Truth of a Theory
> * 28:09 Deep Darwinism and Religion
> * 31:02 Truth as Action
> * 1:00:52 The Intellect
> * 1:04:11 Evolutionary Roots of Western Religion
> * 1:17:36 From Nature to Hero Mythology
> * 1:23:58 Piaget and Pre-rational Morality
> * 1:28:43 Metaphorical Thought
> * 1:36:00 Science and Moral Truth
> * 1:48:17 Evolutionary Psychology and Deep Time
> * 1:49:13 Religious Metaphysics: Optional?
> * 1:55:50 Interlude: Triangulating Meaning
> * 1:58:02 End of Religion?
> * 1:59:18 Reducing Religion & Expanding Evolution
> * 2:01:09 Jung: Studying Information, Not Matter
> * 2:08:16 Metaphor vs. Reality: Not Obvious
> * 2:13:24 Humans: Efficient Complexity Managers
> * 2:21:06 The Multicultural Dimension
> * 2:27:35 From Theory to Action

#### [#20 – Ideology, Logos & Belief](https://www.jordanbpeterson.com/podcast/episode-20/)

June 7th, 2017

> Two-part interview with Transliminal Media, April 2017, in Vancouver, Canada. Sequel to the hit 2015 interview 'Religion, Myth, Science, Truth': https://youtu.be/07Ys4tQPRis 

* [Transcript](https://www.jordanbpeterson.com/transcripts/transliminal/)

#### [#5 – The Psychology of Redemption](https://www.jordanbpeterson.com/podcast/episode-5/)

January 10th, 2017

> A TVO Big Ideas Lecture from 2012, presented at INPM's Conference on Personal Meaning. It discusses the idea of redemption in Christianity from a psychological perspective, comparing in part to ideas of transformation in psychotherapy. Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events

#### [#6 – There's no Such Thing as a Dragon](https://www.jordanbpeterson.com/podcast/episode-6/)

January 10th, 2017

> This is the first Big Ideas Lecture performed by Jordan Peterson, back in 2002. He reads a book for very young children by Jack Kent called "There's no Such Thing as a Dragon" to a group of University of Toronto alumni (most over 65). He

#### [S2 E2: Myth & Reality: General Stanley McChrystal](https://www.jordanbpeterson.com/podcast/s2-e2-myth-reality-general-stanley-mcchrystal/)

April 1st, 2019

> I had the opportunity to speak recently with General Stanley McChrystal, retired four-star general, former Commander of the International Security Assistance Force and Commander, US Forces, Afghanistan. Since 2010, he has taught courses in international relations at Yale University as a Senior Fellow of the University’s Jackson Institute for Global Affairs.

## Maps of Meaning - TVO

#### [#9 – Monsters of Our Own Making](https://www.jordanbpeterson.com/podcast/episode-9/)

February 27th, 2017

> * Part 1: Maps of Meaning: 1 Monsters of Our Own Making.
>   - This is the first of a 13-part 30 minute episode television series broadcast by TVO presenting Dr. Jordan B Peterson’s lectures on his book, Maps of Meaning: The Architecture of Belief. The lecture provides a good introduction to the psychology of mythology and religion, based on the idea that stories from these domains describe the world as a place of action, rather than, as science does, a place of things.
> * Part 2: Maps of Meaning: 2 Contending with Chaos
> * Part 3: Maps of Meaning: 3 Becoming Like Gods

#### [#10 – Games People Must Play](https://www.jordanbpeterson.com/podcast/episode-10/) 

March 6th, 2017

> * Maps of Meaning 4: Games People Must Play – Starting at 0:32
> * Maps of Meaning 5: Grappling with Fear – Starting at 27:59
> * Maps of Meaning 6: Submitting to Order – Starting at 55:23

#### [#12 – Contemplating Genesis](https://www.jordanbpeterson.com/podcast/episode-12/) 

March 20th, 2017

> * Maps of Meaning 7: Contemplating Genesis – Starting at 0:32
> * Maps of Meaning 8: Dwelling on Paradise – Starting at 27:57
> * Maps of Meaning 9: Becoming A Self – Starting at 55:22

#### [#13 – Maps of Meaning 10 – 13](https://www.jordanbpeterson.com/podcast/episode-13/)

March 26th, 2017

> * Maps of Meaning 10: Figuring Evil – Starting at 0:32
> * Maps of Meaning 11: Losing Religion – Starting at 27:57
> * Maps of Meaning 12: Truths that Matter – Starting at 55:22
> * Maps of Meaning 13: The Force Within – Starting at 1:22:45


## God and Religion

> #### [#53 – Susan Blackmore – Do we need God to make sense of life?](https://www.jordanbpeterson.com/podcast/episode-53/)

June 28th, 2018

Jordan Peterson vs Susan Blackmore - Do we need God to make sense of life? Jordan Peterson debated atheist psychologist Susan Blackmore on the UK's Unbelievable? show as part of their Big Conversation series. For the video, more debates and bonus content sign up www.thebigconversation.show Get

#### [#63 – Sir Roger Scruton – Apprehending the Transcendent](https://www.jordanbpeterson.com/podcast/episode-63/)

March 12th, 2019

A conversation between Dr. Jordan Peterson and Sir Roger Scruton, moderated by Dr. Stephen Blackwood, introduced by Professor Douglas Hedley, presented by The Cambridge Centre for the Study of Platonism and Ralston College, held on Nov 2, 2018 in Cambridge, England. God willing (so to speak) I

#### [S2 E14: Bishop Barron: Catholicism and the Modern Age](https://www.jordanbpeterson.com/podcast/s2-e14-bishop-barron-catholicism-and-the-modern-age/)

June 23rd, 2019

For Ep 14, we present Dr. Peterson's highly anticipated conversation with Bishop Barron. Word On Fire: https://www.wordonfire.org/bishop-robert-barron/ 

#### [S2 E15: Who Dares Say He Believes in God](https://www.jordanbpeterson.com/podcast/s2-e15-who-dares-say-he-believes-in-god/)

June 30th, 2019

"I have been asked many times by many people if I believe in God. I don't like this question. I generally respond by stating that I act as if God exists, but that's not sufficiently true. Who could do that? Who could conduct themselves with the

#### [S2 E16: On Claiming Belief in God: Commentary & Discussion with Dennis Prager](https://www.jordanbpeterson.com/podcast/s2-e16-on-claiming-belief-in-god-commentary-discussion-with-dennis-prager/)

July 7th, 2019

Dr. Jordan Peterson delivered this talk at the Prager Summit in May in Santa Barbara. However, it's really an extended commentary on a lecture from Sydney, Australia, Feb 26, 2019, entitled Who Dares Say He Believes in God? To conclude this episode, Jordan also talks with

#### [S2 E18: Religious Belief and the Enlightenment with Ben Shapiro](https://www.jordanbpeterson.com/podcast/s2-e18-religious-belief-and-the-enlightenment-with-ben-shapiro/)

July 21st, 2019

Jordan Peterson's recent discussion with special guest, Ben Shapiro. 

#### [#13 – Maps of Meaning 10 – 13](https://www.jordanbpeterson.com/podcast/episode-13/)

March 26th, 2017

Maps of Meaning 10: Figuring Evil – Starting at 0:32 Maps of Meaning 11: Losing Religion – Starting at 27:57 Maps of Meaning 12: Truths that Matter – Starting at 55:22 Maps of Meaning 13: The Force Within – Starting at 1:22:45 YouTube Video playlist

#### [S2 E2: Myth & Reality: General Stanley McChrystal](https://www.jordanbpeterson.com/podcast/s2-e2-myth-reality-general-stanley-mcchrystal/)

April 1st, 2019

I had the opportunity to speak recently with General Stanley McChrystal, retired four-star general, former Commander of the International Security Assistance Force and Commander, US Forces, Afghanistan. Since 2010, he has taught courses in international relations at Yale University as a Senior Fellow of the University's

## Biblical Lectures

I find Peterson's treatment of Biblical stories to be the most important of Peterson's podcast lectures. I know of no-one else who has drawn such a comprehensive pychological meaning from them, able to speak so profoundly to all, cutting across barriars, regardless of ones spiritual convictions.

#### [#18 – Introduction to the Idea of God](https://www.jordanbpeterson.com/podcast/episode-18/)

May 23rd, 2017

Lecture 1 in my Psychological Significance of the Biblical Stories series from May 16th at Isabel Bader Theatre in Toronto. In this lecture, I describe what I consider to be the idea of God, which is at least partly the notion of sovereignty and power,

#### [#19 – Genesis – Chaos and Order](https://www.jordanbpeterson.com/podcast/episode-19/)

May 29th, 2017

Lecture II in my Psychological Significance of the Biblical Stories from May 23 at Isabel Bader Theatre, Toronto. In this lecture, I present Genesis 1, which presents the idea that a pre-existent cognitive structure (God the Father) uses the Logos, the Christian

#### [#21 – God and the Hierarchy of Authority](https://www.jordanbpeterson.com/podcast/episode-21/)

June 11th, 2017

Lecture 3 from my Psychological Significance of the Biblical Stories Lecture Series. Although I thought I might get to Genesis II in this third lecture, and begin talking about Adam & Eve, it didn't turn out that way. There was more to be said about

#### [#22 – Adam and Eve: Self-Consciousness, Evil, and Death](https://www.jordanbpeterson.com/podcast/episode-22/)

July 1st, 2017

Lecture 4 in my Psychological Significance of the Biblical Stories lecture series. I turned my attention in this lecture to the older of the two creation accounts in Genesis: the story of Adam and Eve. In its few short paragraphs, it covers: 1. the emergence

#### [#23 – Cain and Abel: The Hostile Brothers](https://www.jordanbpeterson.com/podcast/episode-23/)

July 4th, 2017

Bible Series V: Cain and Abel: The Hostile Brothers The account of Cain and Abel is remarkable for its unique combination of brevity and depth. In a few short sentences, it outlines two diametrically opposed modes of being -- both responses to the emergence of

#### [#24 – The Psychology of the Flood](https://www.jordanbpeterson.com/podcast/episode-24/)

July 18th, 2017

Lecture 6 in my Psychological Significance of the Biblical Stories lecture series. The story of Noah and the Ark is next in the Genesis sequence. This is a more elaborated tale than the initial creation account, or the story of Adam and Eve or Cain

#### [#25 – Walking With God: Noah and the Flood](https://www.jordanbpeterson.com/podcast/episode-25/)

August 19th, 2017

Lecture 7 in my Psychological Significance of the Biblical Stories series. Life at the individual and the societal level is punctuated by crisis and catastrophe. This stark truth finds its narrative representation in the widely-distributed universal motif of the flood. Mircea Eliade, the great Romanian

#### [#26 – The Phenomenology of the Divine](https://www.jordanbpeterson.com/podcast/episode-26/)

August 22nd, 2017

Lecture 8 in my Psychological Significance of the Biblical Stories series. In the next series of stories, the Biblical patriarch Abram (later: Abraham) enters into a covenant with God. The history of Israel proper begins with these stories. Abram heeds the call to adventure, journeys

#### [#27 – The Call to Abraham](https://www.jordanbpeterson.com/podcast/episode-27/)

August 31st, 2017

Lecture 9 in my Psychological Significance of the Biblical Stories series. In this lecture, I tell the story of Abraham, who heeds the call of God to leave what was familiar behind and to journey into unknown lands. The man portrayed in the Bible as

#### [#28 – Abraham: Father of Nations](https://www.jordanbpeterson.com/podcast/episode-28/)

September 1st, 2017

Lecture 10 in my Psychological Significance of the Biblical Stories series. The Abrahamic adventures continue with this, the tenth lecture in my 12-part initial Biblical lecture series. Abraham's life is presented as a series of encapsulated narratives, punctuated by sacrifice, and the rekindling of his

#### [#29 – Sodom and Gomorrah](https://www.jordanbpeterson.com/podcast/episode-29/)

September 4th, 2017

Lecture 11 in my Psychological Significance of the Biblical Stories series. Often interpreted as an injunction against homosexuality (particularly by those simultaneously claiming identity as Christians and opposed to that orientation), the stories of the angels who visit Abraham, bless him, and then rain destruction

#### [#30 – The Great Sacrifice: Abraham and Isaac](https://www.jordanbpeterson.com/podcast/episode-30/)

September 9th, 2017

Direct Feed Soundcloud Upload Lecture 12 in the Psychological Significance of the Biblical Stories series In this, the final lecture of the Summer 2017 12-part series The Psychological Significance of the Biblical Stories, we encounter, first, Hagar's banishment to the desert

#### [#33 – Jacob’s Ladder – Part 1](https://www.jordanbpeterson.com/podcast/episode-33/)

November 13th, 2017

The Psychological Significance of the Biblical Stories starts up after a two month hiatus with the first half of the story of Jacob, the founder of Israel ("those who wrestle with God"), the man who robs his brother of his birthright, is deceived

#### [#35 – Jacob: Wrestling with God](https://www.jordanbpeterson.com/podcast/episode-35/)

November 30th, 2017

In this lecture, I present the second half of the story of Jacob, later Israel (he who struggles with God). After serving his time with his uncle Laban, and being deceived by him in the most karmic of manners, Jacob returns to his

#### [#36 – Joseph and the Coat of Many Colors](https://www.jordanbpeterson.com/podcast/episode-36/)

December 21st, 2017

Lecture 14 in my Psychological Significance of the Biblical Stories lecture series. This lecture closes 2017, and the book of Genesis. In it, I present the story of Joseph who, as the wearer of the coat of many colors, is profoundly adaptable, courageous, adaptable, merciful and

## Philosophy

#### [#2 – Tragedy vs. Evil](https://www.jordanbpeterson.com/podcast/episode-2/)

December 18th, 2016

In a lecture recorded by TVO, Professor Jordan Peterson discusses the nature of evil, distinguishing it from tragedy, and presenting his ideas on how both the former and the latter might be most effectively dealt with. Relevant Links 12 Rules for Life Live

#### [#3 – The Necessity of Virtue](https://www.jordanbpeterson.com/podcast/episode-3/)

December 22nd, 2016

A recording of the 2010 Hancock Lecture and was recorded by TVO. Dr Peterson discusses virtue from a contemporary perspective that both encompasses and extends beyond moral and religious contexts. Through compelling stories and research, Dr Peterson illustrates the necessity of virtue both for the individual

#### [S2 E7: Stephen Hicks: Philosophy and Postmodernism](https://www.jordanbpeterson.com/podcast/s2-e7-a-conversation-with-stephen-hicks/)

May 5th, 2019

Today, we’re presenting Dr. Peterson's conversation with Stephen Hicks, recorded on March 27, 2019. Stephen R. C. Hicks is Professor of Philosophy at Rockford University, Illinois, USA, Executive Director of the Center for Ethics and Entrepreneurship, and Senior Scholar at The Atlas Society. https://www.jordanbpeterson.com http://www.stephenhicks.org http://www.stephenhicks.org/2019/04/30/ph-d-philosophers-review-explaining-postmodernism/ http://www.opencollegepodcast.org Relevant

## Culture

#### [#8 – Samuel Andreyev](https://www.jordanbpeterson.com/podcast/episode-8/)

February 19th, 2017

A conversation with Samuel Andreyev: A composer, poet, teacher and performer. His music is performed, broadcast, recorded and written about worldwide, and is known for its expressive intensity, spirit of exploration and enormous range of timbres. Resolutely independent, his compositional process is marked by a

#### [#54 – Aspen Ideas Festival: From the Barricades of the Culture Wars](https://www.jordanbpeterson.com/podcast/episode-54/)

July 6th, 2018

From the Aspen Ideas Festival, recorded Tuesday, June 26, 2018. 

#### [S2 E4: Akira the Don: Music and Meaning](https://www.jordanbpeterson.com/podcast/a-conversation-with-akira-the-don/)

April 15th, 2019

On this episode, we’re presenting a conversation with Akira The Don, a British musician who has used parts of Dr. Peterson's lectures in his music. 

## Interviews

#### [#7 – Gregg Hurwitz](https://www.jordanbpeterson.com/podcast/episode-7/)

February 2nd, 2017

A conversation with Author Gregg Hurwitz covering a variety of topics, including Gregg's recent work, writing practices, perfection vs. wholeness, superheroes & archetypal heroes, how to balance intimacy and work, limits of comedy, free speech, and more. Gregg Hurwitz is the critically acclaimed, New York Times

#### [#14 – A Dialogue with Tom Amarque](https://www.jordanbpeterson.com/podcast/episode-14/)

April 2nd, 2017

Tom Amarque is a German philosopher, writer, publisher, and podcaster. With his podcast 'Lateral Conversations' he seeks out - with the help of a wide range of guests - new developments and perspectives in philosophy, psychology, and spirituality, trying to overcome the pitfalls of what

#### [#15 – How to Change the World](https://www.jordanbpeterson.com/podcast/episode-15/)

April 20th, 2017

Part 1: A Message to Millenials: How to Change the World -- Properly. Young people want, rightly, to change the world. But how might this be properly done? Dr Jonathan Haidt recently contrasted Truth University with Social Justice University. Social Justice U has as its

#### [#16 – An Incendiary Discussion at Ryerson U](https://www.jordanbpeterson.com/podcast/episode-16/)

May 1st, 2017

A few weeks ago, Dr. Oren Amitay, who has been defending me in online discussions hosted by the Ontario Psychological Association, invited me to address his psychology class (to which other students were invited). We discussed freedom of speech, ideological possession, unconscious bias and the

#### [#17 – Dr Martin Daly](https://www.jordanbpeterson.com/podcast/episode-17/)

May 11th, 2017

I'm speaking with Dr. Martin Daly, a professor at McMaster University in Hamilton, Ontario, a pioneer in the field of evolutionary psychology, and author of Killing the Competition (http://amzn.to/2q3pyAO). Dr. Daly has determined that economic inequality and male on male homicide rates are strongly linked,

#### [#31 – Camille Paglia – Modern Times](https://www.jordanbpeterson.com/podcast/episode-31/)

October 19th, 2017

Direct Feed Soundcloud Upload Dr. Camille Paglia is a well-known American intellectual and social critic. She has been a professor at the University of the Arts in Philadelphia, Pennsylvania (where this discussion took place) since 1984. She is the author of

#### [#32 – Compelled Speech – Law Society of Ontario](https://www.jordanbpeterson.com/podcast/episode-32/)

November 3rd, 2017

Direct Feed Soundcloud Upload Part 1 of this podcast is the video "A Call to Rebellion for Ontario Legal Professionals": (http://bit.ly/2yo4Jpe). Part 2 is the video: "Update: Law Society of Ontario Compelled Speech": (https://www.youtube.com/watch?v=FPpPnGA8rkQ) On October 10, Professor Bruce Pardy, Lawyer Jared

#### [#34 – Jonathan Haidt](https://www.jordanbpeterson.com/podcast/episode-34/)

November 21st, 2017

I recently traveled to New York University to talk with Dr. Jonathan Haidt about, among other things, disgust, purity, fear and belief; the perilous state of the modern university; and his work with Heterodox Academy (https://heterodoxacademy.org/) an organization designed to draw attention to

#### [#37 – Deconstruction: The Lindsay Shepherd Affair](https://www.jordanbpeterson.com/podcast/episode-37/)

January 4th, 2018

In November 2017, Wilfred Laurier University teaching assistant Lindsay Shepherd was called to a disciplinary meeting by two professors (Nathan Rambukkana and Herbert Pimlott) and one administrator (Adria Joel) to discuss her screening of a video clip from TVO's The Agenda with Steve

#### [#38 – Cathy Newman and Analysis](https://www.jordanbpeterson.com/podcast/episode-38/)

January 29th, 2018

On January 16th, 2018 Channel 4 aired the now infamous interview between Cathy Newman and Dr. Jordan Peterson. This podcast will first air the original interview and then the interview of Dr. Peterson by Geenstijl that was an analysis of the interview followed with an

#### [#39 – The Master and His Emissary: Dr. Iain McGilchrist](https://www.jordanbpeterson.com/podcast/episode-39/)

February 20th, 2018

While in the UK, recently, I had a chance to sit down for an all-too-short half-hour with Dr. Iain McGilchrist, author of The Master and His Emissary (description below). Our conversation was taped by Perspectiva (http://bit.ly/2EOCiU0), who described it as follows: "An extraordinary half-hour conversation


#### [#41 – Quillette – Discussion with editor Claire Lehmann](https://www.jordanbpeterson.com/podcast/episode-41/)

March 15th, 2018

Please consider supporting Quillette’s fine work at www.patreon.com/Quillette/ove... I sat down recently with Claire Lehmann, founder, and editor of Quillette, an online magazine that publishes essays on a range of topics related to politics, social life, science, and academia. The magazine has quickly become a highly

#### [#45 – Australia’s John Anderson & Dr. Jordan B Peterson: In Conversation](https://www.jordanbpeterson.com/podcast/episode-45/)

April 26th, 2018

I was in Australia in mid-March of 2018, speaking in Sydney, Brisbane and Melbourne. While I was there, I had the privilege of speaking to former Deputy Prime Minister John Anderson (Facebook: https://www.facebook.com/johnandersonao/), who is described accurately on Wikipedia as "handsome, well-educated and well-spoken." Mr. Anderson

#### [#47 – Warren Farrell – The Absolute Necessity of Fathers](https://www.jordanbpeterson.com/podcast/episode-47/)

May 13th, 2018

I came across Dr. Warren Farrell's work a few years ago, when I read Why Men Earn More(https://amzn.to/2HX3Epj), a careful study of the many reasons for the existence of the "gender pay gap," attributed by ideologues of the identity-politics persuasion to systemic patriarchal prejudice and

#### [#50 – Dr. Richard Haier: The Neuroscience of Intelligence](https://www.jordanbpeterson.com/podcast/episode-50/)

June 4th, 2018

There is almost nothing more important to understand about people than intelligence. It can be measured more accurately than anything else in the social sciences. It differs tremendously and importantly between individuals. It is the single most important determinant of life success. It's very existence,

#### [#51 – Russell Brand](https://www.jordanbpeterson.com/podcast/episode-51/)

June 7th, 2018

Russell Brand talked with me in London. This was our second conversation (the first was in LA a few months ago: www.youtube.com/watch?v=kL61yQgdWeM). I thought we had a good discussion then, but I think this one was better. We met in London about two weeks ago (in

#### [#52 – Munk Debate: Political Correctness](https://www.jordanbpeterson.com/podcast/episode-52/)

June 11th, 2018

The resolution? "What you call political correctness, I call progress…" On May 18th, the redoubtable Stephen Fry (self-admitted soft leftie) and I debated the duo of academic, author and radio host Michael Eric Dyson (https://bit.ly/2IzKSZz) and blogger/author Michelle Goldberg (https://bit.ly/2wVOTBZ). A press release describing the

#### [#59 – Bjørn Lomborg](https://www.jordanbpeterson.com/podcast/episode-59/)

December 8th, 2018

On December 7, 2018, I spoke with Dr. Bjorn Lomborg, author and President of Copenhagen Consensus Center, a singularly innovative and influential US-based think tank. Dr. Lomborg and his team have done the hard conceptual and empirical work necessary to turn good intentions for global

#### [#60 – Femsplainers – Christina Hoff Sommers and Danielle Crittenden](https://www.jordanbpeterson.com/podcast/episode-60/)

January 2nd, 2019

On December 13, 2018, I was a guest on Femsplainers with Christina Hoff Sommers and Danielle Crittenden. We discussed, among other topics, the secrets of a long marriage, the problems with dating apps, how to handle a belligerent toddler, and the motivation of my radical

#### [#62 – Our Cultural Inflection Point and Higher Education](https://www.jordanbpeterson.com/podcast/episode-62/)

February 21st, 2019

Dr Jordan Peterson speaks with Dr Stephen Blackwood about Solzhenitsyn, our cultural inflection point, higher education, and the hunger of the young for meaning. Dr Stephen Blackwood is the president of Ralston College, a new university being founded in Savannah, Georgia. Ralston College is committed

#### [S2 E22: Jamil Jivani: Author & Activist](https://www.jordanbpeterson.com/podcast/s2-e22-jamil-jivani-author-activist/)

August 18th, 2019

Jamil Jivani (recorded 01/09/19) joins Dr. Jordan B. Peterson. Jamil is the author of, Why Young Men. About Jamil's Book: Jamil Jivani recounts his experiences working as a youth activist throughout North America and the Middle East, drawing striking parallels between ISIS recruits, gangbangers, and Neo-Nazis

#### [S2 E31: Rex Murphy’s Interview with Jordan B. Peterson](https://www.jordanbpeterson.com/podcast/s2-e31-rex-murphys-interview-with-jordan-b-peterson/)

October 20th, 2019

Legendary Canadian commentator and author, Rex Murphy... interviews Jordan. This episode is truly unique! More from Rex Murphy: https://nationalpost.com/author/rmurphynp 

## Intellectual Dark Web

#### [#48 – Ben Shapiro](https://www.jordanbpeterson.com/podcast/episode-48/)

May 14th, 2018

Shapiro discussion starts at around 4:20 if you want to skip the tour information. This is an alternate posting of Ben Shapiro's May 6 premiere Sunday Special discussion with me (bit.ly/2I4czoU). Ben and I had a very intense discussion about the nesting of human perception

#### [#49 – Steven Pinker: Enlightenment Now](https://www.jordanbpeterson.com/podcast/episode-49/)

May 28th, 2018

I spoke with Harvard's Dr. Steven Pinker about the immense improvements in human living conditions that are now happening with amazing speed almost everywhere in the world -- as detailed in his new book, Enlightenment Now (https://amzn.to/2jDwv7D) -- a careful, clear-headed and data-driven defense of

#### [#56 – Gregg Hurwitz – An Invitation to the Intellectual Dark Web](https://www.jordanbpeterson.com/podcast/episode-56/)

August 30th, 2018

I spoke today with author Gregg Hurwitz (more information about him below). Hurwitz is currently working to rebrand the Democratic party, working to produce an issues-oriented, anti-corruption, practical mainstream narrative and to identify promising Democratic candidates who can hold their own in long form political

#### [S2 E11: Who is Joe Rogan? Part One](https://www.jordanbpeterson.com/podcast/s2-e11-a-conversation-with-joe-rogan-part-one/)

June 2nd, 2019

Part one of Dr. Jordan B. Peterson's interview with Joe Rogan. Joe Rogan is an American stand-up comedian, mixed martial arts (MMA) color commentator, podcast host, businessman, television host, and actor. 

#### [S2 E12: Who is Joe Rogan? Part Two](https://www.jordanbpeterson.com/podcast/s2-e12-a-conversation-with-joe-rogan-part-two/)

June 9th, 2019

Part two of Dr. Jordan B. Peterson's interview with Joe Rogan. Joe Rogan is an American stand-up comedian, mixed martial arts (MMA) color commentator, podcast host, businessman, television host, and actor. 

#### [S2 E20: Progress, Despite Everything](https://www.jordanbpeterson.com/podcast/s2-e20-progress-despite-everything/)

August 4th, 2019

Steven Pinker returns for a conversation with Dr. Jordan B. Peterson. Steven Arthur Pinker is a Canadian-American cognitive psychologist, linguist, and popular science author. He is a professor in the Department of Psychology at Harvard University, and is known for his advocacy of evolutionary psychology and the computational theory of mind. 

## Self Authoring 
#### [#11 – Dr. James W. Pennebaker](https://www.jordanbpeterson.com/podcast/episode-11/)

March 9th, 2017

Dr. James W. Pennebaker is Professor of Psychology at the University of Texas at Austin, and the Executive Director of Project 2021, aimed at rethinking undergrad education at that university. I first encountered Dr. Pennebaker's work when I was working on the Self Authoring Suite,

## 12 Rules for Life

#### [#40 – Message to the school shooters: past, present and future](https://www.jordanbpeterson.com/podcast/episode-40/)

March 12th, 2018

I wrote in some detail and with some depth about motivation for the mass slaughter of innocents in my new book, 12 Rules for Life: An Antidote to Chaos. Because of what happened all-too-recently and brutally in Parkland -- because of what keeps happening --

#### [#43 – Jordan Peterson’s Rules for Life with Richard Fidler](https://www.jordanbpeterson.com/podcast/episode-43/)

April 2nd, 2018

From Dr. Peterson's appearance on Conversations with Richard Fidler from March 14th, 2018. Richard Fidler is an Australian ABC radio presenter, and writer, best known for his hour-long interview program, Conversations with Richard Fidler. The program is ABC Radio's most popular podcast, downloaded more than 3 million times per month. It features

#### [#57 – Dr. Oz – Jordan Peterson’s Rules to Live By](https://www.jordanbpeterson.com/podcast/episode-57/)

October 22nd, 2018

In this exclusive, in-depth interview, author and clinical psychologist Dr. Jordan Peterson joins Dr. Oz to discuss how we can find meaning in our lives, challenge our thinking, and provide tactical ways we can reach our full potential. Take Dr. Peterson's full personality quiz: https://www.understandmyself.com Dr.

#### [S2 E1: 12 Rules Seattle: Facts, Stories, and Values](https://www.jordanbpeterson.com/podcast/s2-e1-from-the-moore-theatre-in-seattle-wa/)

March 25th, 2019

For this episode, we’re presenting Dr. Peterson’s 12 Rules for Life Tour lecture at the Moore Theatre in Seattle, WA on June 21, 2018. The lecture covers the evolution of religion thinking, a true human universal. Everyone has to deal with the problem of value. Everyone has to

#### [S2 E3: 12 Rules Portland: Technology, Temperament & Free Speech](https://www.jordanbpeterson.com/podcast/s2-e3-lecture-from-portland-or-june-25-2018/)

April 9th, 2019

In this lecture, I describe the surprising popularity of long-form philosophical discussions, making reference to my talks with Sam Harris on science and value and religion and atheism, the vital and biologically-influenced role that temperament and personality play in determining individual interest and ability, and the

#### [S2 E5: 12 Rules Sacramento: The Sovereignty of the Individual](https://www.jordanbpeterson.com/podcast/s2-e5-lecture-sacramento-june-27-2018/)

April 21st, 2019

For this episode, we’re presenting Jordan’s lecture at The Community Theatre in Sacramento, California on June 27, 2018. Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events My new book: 12 Rules for Life: An Antidote to Chaos: jordanbpeterson.com/12-rules-for-life My first book: Maps of Meaning: The


#### [S2 E6: 12 Rules Thousand Oaks: Truth in Speech, and Meaning](https://www.jordanbpeterson.com/podcast/s2-e6-lecture-the-civic-arts-plaza-in-thousand-oaks-ca/)

April 28th, 2019

For this episode, we’re presenting Jordan’s lecture at the Civic Arts Plaza in Thousand Oaks, California on June 30, 2018. Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events My new book: 12 Rules for Life: An Antidote to Chaos: jordanbpeterson.com/12-rules-for-life My first book: Maps of

#### [S2 E8: 12 Rules Hamilton: Courage](https://www.jordanbpeterson.com/podcast/s2-e8-lecture-hamilton-ontario-july-20-2018/)

May 12th, 2019

Lecture: Hamilton, Ontario - July 20, 2018 Today, we're presenting Dr. Jordan B. Peterson's 12 Rules for Life Lecture at the First Ontario Concert Hall in Hamilton, Ontario. https://www.jordanbpeterson.com https://twitter.com/jordanbpeterson https://twitter.com/MikhailaAleksis https://mikhailapeterson.com Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events My new book: 12

#### [S2 E9: Milo Yiannopoulos: Past, present and future](https://www.jordanbpeterson.com/podcast/s2-e9-a-conversation-with-milo-yiannopoulos/)

May 19th, 2019

Today we present Dr. Peterson's conversation with journalist, performance artist, and comedian, Milo Yiannopoulos, recorded April 11, 2019. https://www.jordanbpeterson.com https://www.youtube.com/channel/UC6Hyj5sVbiYhpc_FA14nziw Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events My new book: 12 Rules for Life: An Antidote to Chaos: jordanbpeterson.com/12-rules-for-life My first book: Maps of

#### [S2 E10: 12 Rules Vancouver: Five levels of chaos and order](https://www.jordanbpeterson.com/podcast/s2-e10-lecture-vancouver-july-26-2018/)

May 26th, 2019

Today, we’re presenting Dr. Peterson's 12 Rules for Life lecture at the Chan Centre in Vancouver, British Columbia, recorded on July 26, 2018. 

#### [S2 E13: 12 Rules Kitchener: Hierarchy and Fair Play](https://www.jordanbpeterson.com/podcast/s2-e13-12-rules-for-life-lecture-kitchener-ontario/)

June 16th, 2019

Dr. Jordan B. Peterson's "12 Rules For Life" lecture at the Center in the Square in Kitchener, Ontario, recorded on July 21, 2018. 

#### [S2 E17: 12 Rules: London, Ontario: The inevitability, utility and danger of hierarchies](https://www.jordanbpeterson.com/podcast/s2-e17-12-rules-london-ontario-the-inevitability-utility-and-danger-of-hierarchies/)

July 14th, 2019

Jordan Peterson's 2018 lecture on the 12 Rules for life from London, Ontario. 

#### [S2 E19: The Main Themes – 12 Rules for Life](https://www.jordanbpeterson.com/podcast/s2-e19-the-main-themes-12-rules-for-life/)

July 28th, 2019

Jordan Peterson's 12 Rules for Life lecture from Ottawa on July, 23 2018. 

#### [S2 E21: Territory, Hierarchy, Security, and Fear](https://www.jordanbpeterson.com/podcast/s2-e21-territory-hierarchy-security-and-fear/) - [[transcript](JP-S2-EP-21-Territory-Hierarchy-Security-Fear_Calgary-AB.md)]

August 11th, 2019

Territory, Hierarchy, Security, and Fear Jordan B. Peterson's "12 Rules for Life" lecture from Calgary, AB 

#### [S2 E23: Why You Should Treat Yourself as if You Have Value](https://www.jordanbpeterson.com/podcast/s2-e23-why-you-should-treat-yourself-as-if-you-have-value/)

August 25th, 2019

Jordan's 12 Rules for Life lecture from July, 28 2018 in Edmonton, AB. 

#### [S2 E24: The World is Your Oyster](https://www.jordanbpeterson.com/podcast/s2-e24-the-world-is-your-oyster/)

September 1st, 2019

Jordan's 12 Rules for Life lecture from Winnipeg. Recorded July of 2018. 

#### [S2 E25: Be precise in your speech](https://www.jordanbpeterson.com/podcast/s2-e25-be-precise-in-your-speech/)

September 8th, 2019

Jordan's 12 Rules for Life tour continues from Regina SK. Recorded August 14, 2018. Dr. Peterson focuses on Rule 10. 

#### [S2 E26: Aim at a Star – A 12 Rules for Life Lecture](https://www.jordanbpeterson.com/podcast/s2-e26-aim-at-a-star-a-12-rules-for-life-lecture/)

September 15th, 2019

Dr. Peterson's 12 Rules for Life lecture in Rochester, NY. Recorded Sept. 5th, 2018 

#### [S2 E27: Risk Being a Fool: A 12 Rules for Life Lecture](https://www.jordanbpeterson.com/podcast/s2-e27-risk-being-a-fool-a-12-rules-for-life-lecture/)

September 22nd, 2019

Jordan Peterson's 12 Rules for Life lecture from Westbury, NY. 

#### [S2 E28: Responsibility is the Key to Meaning](https://www.jordanbpeterson.com/podcast/responsibility-is-the-key-to-meaning/)

September 29th, 2019

Dr. Peterson visits Texas for a lecture on his 12 Rules for Life! This episode is a standout from his 2018 tour. 

#### [S2 E29: You have an Endless Potential](https://www.jordanbpeterson.com/podcast/s2-e29-you-have-an-endless-potential/)

October 6th, 2019

Jordan Peterson's 12 Rules for Life lecture from Dublin, Ireland. Recorded on Oct 2018. 

#### [S2 E30: The Crisis of Masculinity](https://www.jordanbpeterson.com/podcast/s2-e30-the-crisis-of-masculinity/)

October 13th, 2019

The Crisis of Masculinity is a lecture from Jordan's 12 Rules for Life Tour on Oct. 25, 2018 in Manchester. 

#### [S2 E32: The Topic of Truth](https://www.jordanbpeterson.com/podcast/s2-e32-the-topic-of-truth/)

October 27th, 2019

A standout 12 Rules for Life lecture from Dr. Jordan B. Peterson. Recorded in Edinburgh 10-28-2018. 

#### [S2 33: Set Your House in Order](https://www.jordanbpeterson.com/podcast/s2-33-set-your-house-in-order/)

November 3rd, 2019

Set Your House in Order is a 12 Rules for Life lecture from Jordan B. Peterson. Recorded Nov. 1, 2018 in Cambridge UK. 

#### [S2 E34: Competence Hierarchies](https://www.jordanbpeterson.com/podcast/s2-e34-competence-hierarchies/)

November 10th, 2019

"Competence Hierarchies" is a Jordan Peterson 12 Rules for Life lecture recorded in Birmingham UK (November of 2018). 

## Q/A

#### [#42 – March 2018 Q & A](https://www.jordanbpeterson.com/podcast/episode-42/)

March 19th, 2018

Every month, I answer questions from the generous people who support me on Patreon. This is the Patreon Q & A from March 2018. 0:48 Q#1 - What should teaching and research in the humanities look like? I'm going to grad school for literature and

#### [#44 – Lafayette College – The Mill Series](https://www.jordanbpeterson.com/podcast/episode-44/)

April 20th, 2018

Lafayette College on March 29th, 2018, hosted and sponsored by The Mill Series. Reposted from The Mill Series Channel: https://www.youtube.com/channel/UCvcp... Original Video: https://www.youtube.com/watch?v=qT_YS... Relevant Links 12 Rules for Life Live Tour: www.jordanbpeterson.com/events My new book: 12 Rules for Life: An Antidote to Chaos: jordanbpeterson.com/12-rules-for-life My

#### [#46 – April 2018 Q & A](https://www.jordanbpeterson.com/podcast/episode-46/)

May 8th, 2018

Every month, I answer questions from the generous people who support me. This is the Q & A from April 2018. 1. 2:52 : Free will; Sam Harris's opinions on free will 2. 16:37 : Chaos vs. order 3. 31:24 : Gay couples raising children

#### [#55 – August 2018 Q & A](https://www.jordanbpeterson.com/podcast/episode-55/)

August 20th, 2018

Every month (or as close as I can get), I answer questions from the generous people who support me on Patreon. This is the Patreon Q & A from August 2018. 0:00:57 – Announcements 0:06:33 – (Q#1) How are you? 0:12:04 – (Q#2) What can

#### [#58 – October 2018 Q & A](https://www.jordanbpeterson.com/podcast/episode-58/)

November 6th, 2018

[1] 0:01:23 – Release of 50th Anniversary version of The Gulag Archipelago with a forward written by JBP [2] 0:03:11 – Finalized deal for next book “12 More Rules: Beyond Mere Order” [3] 0:04:11 – ‘12 Rules’ tour plans for the next year 0:07:39 – (Q#1) Any advice for young a young

#### [#61 – January 2019 Q & A](https://www.jordanbpeterson.com/podcast/episode-61/)

January 22nd, 2019

Happy New Year. This is the first of this year's Q and A's. I am going to answer audience questions, as usual, as well as discussing my next book, my impending departure from Patreon, and my upcoming tour to Switzerland, California, Australia and New Zealand.

