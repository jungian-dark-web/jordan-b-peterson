# Psych 230h - Personality and its Transformations - Introduction and Overview

* [2017 Personality 01: Introduction](https://www.youtube.com/watch?v=kYYJlNbV1OM)

> In this lecture, I define personality from multiple angles, historical, constructivist, psychoanalytic, behavioral and neuropsychological. Personality is a way of looking at the world, and a characteristic mode of behaving. It's both stable and adaptively dynamic. 

* [Psychology 230H](https://www.jordanbpeterson.com/classes/personality-and-its-transformations/)

#### Further Study

1. Gerald M. Edelman
* [wikipedia.org/wiki/Gerald_Edelman](https://en.wikipedia.org/wiki/Gerald_Edelman)
* [Building a Picture of the Brain](https://www.jstor.org/stable/20027490) *Daedalus - Vol. 127, No. 2, The Brain (Spring, 1998), pp. 37-69* (still need to verify relevance\immediacy)

2. Big Five Personality Model
* [Big Five Personality Model](https://www.truity.com/book/big-five-personality-model) (high level, quick read)
* [wikipedia.org/wiki/Big_Five_personality_traits](https://en.wikipedia.org/wiki/Big_Five_personality_traits)
  * [Openness to experience](https://en.wikipedia.org/wiki/Openness_to_experience) (inventive/curious vs. consistent/cautious)
  * [Conscientiousness](https://en.wikipedia.org/wiki/Conscientiousness) (efficient/organized vs. easy-going/careless)
  * [Extraversion](https://en.wikipedia.org/wiki/Extraversion_and_introversion) (outgoing/energetic vs. solitary/reserved)
  * [Agreeableness](https://en.wikipedia.org/wiki/Agreeableness) (friendly/compassionate vs. challenging/detached)
  * [Neuroticism](https://en.wikipedia.org/wiki/Neuroticism) (sensitive/nervous vs. secure/confident)

> The five factors are represented by the acronym OCEAN or CANOE. Beneath each proposed global factor, there are a number of correlated and more specific primary factors. For example, extraversion is said to include such related qualities as gregariousness, assertiveness, excitement seeking, warmth, activity, and positive emotions.

3. Paretto Distribution

* [wikipedia.org/wiki/Pareto_principle](https://en.wikipedia.org/wiki/Pareto_principle)
* [Google Analytics Pareto Analysis](https://linpack-for-tableau.com/data-visualizations/tableau-dashboards/google-analytics-dashboard/custom-analysis-pareto/)
  ![](https://linpack-for-tableau.com/uploads/packs/000003/ga%20Pareto.PNG?8717)

4. St. George and the Dragon

* [wikipedia.org/wiki/Saint_George_and_the_Dragon](https://en.wikipedia.org/wiki/Saint_George_and_the_Dragon)

5. Hero's Journey

* [wikipedia.org/wiki/Hero%27s_journey](https://en.wikipedia.org/wiki/Hero%27s_journey)

6. *In sterquiliniis invenitur*
* [Mysterium Coniunctionis](https://issuu.com/jihadallam/docs/14_mysterium_coniunctionis__collect)
* [The pragmatics of meaning](https://semioticon.com/frontline/jordan_b.htm)
  > Medieval alchemical thought, serving as a bridge between the extreme spiritualism of European Christianity and the later materialism of science, took to itself the dictum in sterquiliniis invenitur – in filth it shall be found (Jung, 1967, p. 35). In sterquiliniis invenitur comprised the summary statement for a set of beliefs that apparently arose spontaneously among those who were seeking perfection, or the means to perfection, in pursuit of the philosopher’s stone. This set of beliefs was predicated on the assumption or the discovery that the seeds of what redeems were to be found within what was frightening and upsetting (read: anomalous) – and, therefore, within what had been devalued or ignored, precisely because it was frightening or upsetting. 

7. Dimensions of Creativity
* [Reliability, Validity, and Factor Structure of the Creative Achievement Questionnaire](https://www.researchgate.net/publication/234822027_Reliability_Validity_and_Factor_Structure_of_the_Creative_Achievement_Questionnaire)

### Transcript

:27  
The syllabus will tell you pretty much everything you need to know about the class.

0:33  
The there's a text. It consists of readings from a larger text. I use it because it's old, but a lot of the theorists that we're going to talk about are also old. And so I found this particular text accurate. Many of the people that we're going to talk about have very sophisticated views of personality. 

1:09  
I think that it does you a disservice, unless you read something that's sufficiently sophisticated, so that you actually understand, at least to some degree, what the people that we're going to study were talking about. 

#### Index

* [A Definition of Personality](#a-definition-of-personality)
* [Engineering of the Human Spirit](#engineering-of-the-human-spirit)
* [Personality Transformation](#personality-transformation)
* [Big Five Personality Model](#big-five-personality-model)
* [Come up with a Plan for the Course](#come-up-with-a-plan-for-the-course)
* [Creative Achievement Questionnaire](#creative-achievement-questionnaire)
* [Other Elements of Personality](#other-elements-of-personality)
* [Journey to the Underworld](#journey-to-the-underworld)
* [Practicalities](#practicalities)

#### A Definition of Personality

1:29  
We might as well start, I suppose, with a definition of personality. It's hard to define something that's that General, because when you're speaking about human beings, it's not that simple to figure out what constitutes personality and what constitutes something else.

1:53  
I'm going to hit at it from a couple of different perspectives. And while I'm doing that, I would like you also to consider the nature of what you're going to learn. 

2:10  
Human personality is essentially unfathomable.

2:14  
Human beings are unbelievably complicated, and we're nested in systems that are also unbelievably complicated. There are more patterns of connections between neurons in your brain than there are subatomic particles in the Universe by a substantial margin. You can look up Gerald Edelman if you want to find out about that. It's not unreasonable to point out that you're the most complicated thing we know of by many orders of magnitude. And the probability that you can understand yourself in anything approaching totality is extraordinarily low. So this makes the study of personality something very daring and hopeless and complicated. 

3:10  
We're going to cycle through a very large number of theorists. And what you'll find is that although there are commonalities between them, there are market differences. So then you might ask yourself, well, what's the point of studying this sequence of theorists and ideas if there's no point of agreement between them?

3:35  
I would say first, there are points of agreement between, although, personality hasn't advanced to the point where I would say that we have a homogenous theory that's free of internal contradictions. I would also say that personality is a hybrid discipline. Partly science. But it's partly engineering.


#### Engineering of the Human Spirit

4:06  
The clinical element of it, I would say is more like engineering. And what engineers do is try to do things. They try to make something happen. And they are informed by theory. But the point is still to build something. When you're working as a clinical psychologist... and most of the initial theorists that we'll discuss, for the first... little more than a third to a half of the course are clinical theorists. They're trying to build something, and they're dealing with very, very difficult conceptual problems. Because they're either trying to cure mental disorders, or maybe even unhappiness, and trying to bring about health.

4:50  
And the problem with that is that it's not a straightforward thing to define a mental disorder from a scientific perspective. What's healthy, mentally, and what's not, is partly social judgment, and it's partly socially constructed, and it partly has to do with norms, and it partly has to do with ideals... because you might also say that to be healthy is to be normal.

5:21  
You could also say that to be healthy is to be ideal. And then of course, you run into the problem of having to conceptualize an ideal. It isn't self evident that science is capable of conceptualizing an ideal, because ideals tend to fall into the domain of moral judgments, or philosophical judgments rather than scientific judgments, per se. 

5:51  
So what I would say to you is that it would be worthwhile to approach this course as If you are an engineer of the human spirit. An engineer of your own spirit to begin with, but also an engineer of the spirits of other people. Because as you interact with other people, you inevitably tell them what you want, and what you don't want. When they give you what you want, and what you admire, you respond positively to them. You pay attention to them, you smile at them, you focus. You focus your thoughts on them, you interact with them, and you reward them for acting in a particular manner. When they don't respond the way that you want, then you punish them with a look or by turning away or by rejecting their friendship or when you were a child by refusing to play for them, play with them.

6:53  
So we're engaged in the CO creation of personalities. Our own and others. And that also brings up the same question. What is it that we are all collectively trying to be and trying to create?

7:17  
I suspect that you all have the experience of falling short of the ideal, an ideal that you hold for yourself or an ideal that other people hold for you. I suspect that you all feel the negative consequences of falling short of that ideal. Freud would conceptualize that as the super ego imposing its judgment on the ego, you being the ego, the super ego being a hybrid, I suppose, of external external forces, and also your internalization of those judgments and forces.

8:04  
Now personality, I would say has these elements of ideal and has structural elements in a way as well. And we're going to talk about those more in the second half of the class. The structural elements can be lined up and outlined more scientifically. The second half of the class concentrates more on physiology, brain physiology, and on statistical approaches to the description of personality.

8:40  
I suppose you might say that that outlines the territory.

#### Personality Transformation

8:45  
The course is called personality and its transformations because we have personalities, that's who you are now. But our personalities are also capable of transformation, of change. We, we think about that as learning. Some of that might be regarded as factual learning. And some of it might be regarded as learning how to perceive and behave. 

9:18  
The clinical psychologists that will cover to begin with, are much more concerned with the nature of the implicit structures that shape your perceptions and also the implicit structures that shape your behaviors and how they're integrated in relationship to your negative emotion, health and well being. Whereas the thinkers in the second half are more concerned about laying out the structural elements of those features and relating them to underlying say mechanistic phenomena. Making the assumption, which seems warranted, that there's some relationship between your personality and the manner in which your brain functions.

10:10  
I'm going to try and provide you with a meta narrative that will help you unite these different theories. I've often found it useful when I'm trying to remember something to have a story to hang the facts on. Otherwise, you're faced with the necessity of doing nothing but memorization and it isn't obvious to me that memorization actually constitutes knowledge. What constitutes knowledge is that generation of a cognitive structure that enables you to conduct yourself more appropriately in life. 

10:51  
You could argue that a course in psychology, especially in personality, is a course in applied wisdom, as well. Assuming that wisdom is in part your capacity to understand yourself so that you don't present too much of an intolerable mystery to yourself, and also to understand others, so that you can predict their behavior, understand their motivations, negotiate with them, listen to them, and formulate joint games with them so that you can integrate yourself reasonably well with another person and with a family and in society.

#### Big Five Personality Model

11:39  
The structural elements of personality might be regarded as the implicit structures that govern your perception, that tilt you towards certain kinds of behaviors. I can give you some examples we can talk about The Big Five model just briefly. The Big Five personality model is statistical model which will cover in detail, trait by trait, partly during the second half of the course.

12:08  
The Big Five was generated was generated over about 50 years, that personality psychologists gathered together adjectives within the English language first that were used to describe human beings, as many adjectives as they could collect, and then subjected them to a process called factor analysis. And what factor analysis does is enable us statistically to determine, in some sense how similar adjectives are to one another. So for example, if you gave thousand people, a list of adjectives to describe themselves with and one of the adjectives was happy, and another of the adjectives was social, you'd find that those who rated themselves high on happy would also rate themselves high on social, and those who rated themselves low on happy would also rate themselves low on social. 

13:06  
And by looking at those patterns of Co-variation, you can determine what the essential dimensions are of human personality. One of the dimensions is roughly happiness, that's extraversion. Another dimension is neuroticism. It's a negative emotion dimension. So if you ask someone, if they're anxious, and they score high, say, on a scale of one to seven, they're also likely to score high on another item that says that they're sad. And it turns out that negative emotions clumped together, and so that people who experience more of one negative emotion, have a propensity to experience more of all of them. 

13:49  
There's another dimension called agreeableness. And agreeable people are self sacrificing, compassionate and polite. If you're dealing dealing with an agreeable person. They don't like conflict. They care for other people. If you're dealing with an agreeable person, they're likely to put your concerns ahead of theirs. They're non competitive and cooperative. It's a dimension where women are women score more highly than men on agreeableness across cultures, including those cultures where the largest steps have been taken towards producing an egalitarian social circumstance like Scandinavia, actually, the gender differences in personality there are larger than they are anywhere else. 

14:37  
Another trait is conscientiousness. Conscientiousness is an excellent trait if you want to do well in school and in work, especially if you're a manager or an administrator. I can't say we understand a lot about conscientiousness, although it it reliably emerges from factor analytic studies of adjective groups across different countries. conscientious people are dilligent industrious and orderly. Their orderliness tilts them towards political conservatism, by the way, because it turns out that your inbuilt temperament, your inbuilt personality, which constitutes a set of filters through which you view the world also alters the manner in which you process information and influences the way that you vote. 

15:22  
You might say, and I do believe that this is true, or we've been doing a lot of research on this as of late. The more accurate measure you take of someone's political beliefs, the more you find that personality is what's predicting them. And I think that's a reasonable thing to think about. Because, you have to, you have to figure out ways of simplifying the world, right, because you just can't do everything. And so people are specialized, they have specialized niches that they occupy, you can think about them as social niches. Niches, a place where your particular skills would serve to maintain you, and if you're a extroverted, you're going to look for a social niche because you like to be around people. And if you're introverted, you're going to spend much more time on your own. And so if you're an introverted person, for example, you're going to want a job, where you're not selling, and where you're not surrounded by groups of people who are making social demands on you all the time, because it will wear you out. Whereas if you're extroverted, that's just exactly what you want. 

16:23  
So the extrovert sees the world as a place of social opportunity. And the introvert sees the world as a place to retreat from and spend time alone. And it turns out that both of those modes of being are valid, the issue, at least to some degree is whether or not you're fortunate enough to match your temperament with the demands of the environment. And I suppose also whether you're fortunate enough, fortunate enough, so that you're born in an era where there actually is a niche for your particular temperament, because it isn't necessarily the case that that will be the case. 

16:57  
Imagine that. All of these temperamental Dimensions vary because of evolutionary pressure, right? So there's a distribution of extraversion, a normal distribution, most people are somewhere in the middle. And then as you go out towards the extremes, there are fewer and fewer people. And what that means is that on average, across large spans of time, there have been environments that match every single position on that distribution, with most most of the environments matching the center, because otherwise we wouldn't have evolved that way. And so sometimes being really extroverted is going to work well for you in a minority of environments and minority of niches. And sometimes it's just going to be a catastrophe. 

17:37  
I suspect, for example, that if you live in a tyrannical society, where any sign of, personally oriented activity is likely to get you in trouble that being extroverted and low and neuroticism wouldn't be a very good idea because you're going to be mouthy and happy and saying a lot of things, unable to keep your thoughts to yourself, and you're going to be relatively fearless. Now I don't know that for sure because we haven't done the studies that precisely match temperamental proclivity to environmental demand, but you get what I mean.

18:12  
Conscientious people anyways, conscientious people are industrious, and orderly. We know a little bit about orderliness. It seems to be associated, strangely enough with disgust sensitivity, which I suppose isn't that surprising. You take an orderly person and you put them in a messy kitchen, they respond with disgust and want nothing more than to straighten it all out and organize it and clean it. And there's tremendous variability and orderliness. And as I said, orderliness predicts political conservatism. It's not the only thing but it's certainly one of the things. 

18:53  
The correlation between conscientiousness and grades is about point four. It's about 16% of the variants, it's it's the second best predictor of university grades after intelligence, we'll talk about intelligence during this course too. Intelligence is actually relatively straightforward concept. I don't think I'll get into it today, but conscientious people their industriousness and their orderliness makes them schedule their time. So they make efficient use of their time they use schedules and that sort of thing. 

19:23  
We haven't been able to figure out anything about the underlying biology or psychology of industriousness. We've tried, really dozens and dozens of tests, attempting to find a laboratory measure on which industrious people do better and we failed completely. And there's no animal models of industriousness, either. And so I would say it's a great mystery that remains at the heart of trait psychology. And maybe it's a human specific category. You can think of sled dogs maybe of being industrious and maybe, maybe sheep, dogs and animals that work like that. But of course, they've been trained by human beings. But it isn't obvious that animals are industrious the same way we are. I mean, industriousness involves sacrificing the present for the future, something like that. And it seems like you have to be able to conceptualize time, in order to sacrifice the present for the future. 

#### Come up with a Plan for the Course

20:17  
One of the things that I would recommend that you do as students, in this course, and maybe in every course... speaking of industriousness... is come up with a plan of attack for the course, and use a scheduler. If you treat your university career like a full time job, you're much more likely to succeed. And if you keep up on the readings, and you keep up on the, on the essays and all of that, then you're much less likely as well to fall into despair when you get too far behind. Using a Google calendar or something like that to organize a schedule for the entire semester, at the beginning of the semester can be invaluable, especially if you're not ready industriousness, very industrious because it can keep you on track. And one of the things we know about industrious people is that they are very good at using schedules and it planning the use of their time. 

21:12  
I would like to say that you should all be smarter, but I don't know how you could be smarter. We don't know anything about how to improve intelligence. And I suppose we don't really know anything about how to improve industriousness, either, but I can tell you that people who are industrious, come up with a strategy for solving the problem that's ahead of them. And then they do whatever they can to stick to the strategy. For example, if you sat down today or tomorrow, for a couple of hours, three hours and you filled in a Google Calendar, whatever you happen to use with a strategy for studying and a list of when all your assignments are due, and all of that, and when you're going to sit down and study, then you won't be in a position where you have to cram for 10 hours a day hopelessly, right before an important exam. 

22:01  
That's also a very ineffective way of studying, by the way. I mean, first of all, people who cram for 10 hours say they're studying for 10 hours, but they rarely are because, well, I can't study for 10 hours, I don't have the power of concentration that would enable me to do that for that prolonged period of time, I can manage about three hours of intense intellectual activity before I'm pretty done. And it's also the case that if you study and then sleep, and then study and then sleep, and then study and then sleep, you space it out, then you're much more likely to remember, it's also much more likely that you're you're much more likely to remember if you try to recall the material. 

22:39  
Highlighting and that sort of thing isn't very useful. But reading, closing the book, summarizing what you've read without opening the damn book. That's useful. And the reason for that is that you're practicing remembering, and that's what you have to practice. If you're practicing memorization, you have to practice remembering, you don't just go over and over, that'll help you with recognition memory. But some, but it won't help you with recall memory. 

23:07  
The last trade is openness. Openness is a creativity trait. It's also associated with intelligence in that intelligent people and I'm speaking technically of IQ tend to be higher and tend to be more creative, which is hardly surprising. Creative people are more likely to be liberal politically, by the way. They like novelty, they like aesthetics, they like fiction. They like movies they like are they like poetry, there's something about them, that grants them an aesthetic sensitivity. And, and that's a that's an inbuilt trait. And it's not the case, by the way that everyone's creative. In fact, far from it. We've used the creative achievement questionnaire to to measure people's creativity. I'll talk to you about that later in the class.

#### Creative Achievement Questionnaire

23:55  
The creative achievement questionnaire takes 13 dimensions of creativity: writing, dancing, acting, scientific investigation, entrepreneurial activity, architectural activity, cooking. singing, etc. You know the sorts of things that you would assume that people could be creative about. And then it asks people to rate themselves on a scale from one to 10 on their level of achievement. With regards to all those creative domains with zero being, I have no training or proficiency in this area and 70% of people scores zero across the entire creative achievement questionnaire. A tiny proportion of people are outliers way out there creative in many dimensions simultaneously and exceptionally creative. It turns out as you'll find out that that pattern, which is called a pareto distribution, where most people stack up at zero and a few people are way out on the creative and characterizes all sorts of distributions like the distribution of money for example, Which is why 1% of the people have the overwhelming majority of the money. It's a different 1% across time, it churns. And you're much more likely to be in the 1% if you're older, logically enough, because one of the things you do as you age is you trade youth for money. If you're fortunate, I don't think the trade is really worth it, but that's the best you've got.

25:26  
Those particular traits, you can think of those as ways that you simplify the world, right? There's lots of different places that you can act in the world and there's lots of different ways you can look at it and survive. That's why you can be a plumber, and a lawyer and an engineer. And those all work, right, even though they're very different modes of being and you can have different personalities and survive as long as you're capable of finding the place where your particular filters and behavioral proclivities match the demands of the environment. A huge part, I would say, of successful adaptation is precisely that. 

#### Other Elements of Personality

26:07  
There are other elements of personality too. One of the things that I've been struck by and this is actually one of the criticisms I have of the psychoanalysts and the clinicians in general, even though I have great admiration for them, and would say that what they have to say is very much worth listening to, is that it's not obvious that your personality is inside you.

26:29  
A human being is a strange, multi level thing. And you might ask yourself, well, for example, know Is your mother more a part of you then your arm, or maybe even more precisely is your child more a part of you then your arm? It certainly people will do drastically self sacrificing things to maintain the lives of their children. And so you're, you're a person and you're made out of all sub sort of sub components of a person None of which you can see when you look at a person, all the complicated machinery inside you that makes you who you are. And then outside of that, of course, you're nested in all sorts of complex systems. So you're part of a family and, and you're part of a community. And that's part of a province. And that's part of the state. And that's part of international consortium of states. And that's part of an ecosystem and how you make a distinction between you, and the systems that you're embedded in, is also an extraordinary difficulty. 

27:33  
One of the things that you have to do as a clinical psychologist, for example, if you're trying to diagnose someone with depression, you think you think, well, this person's dreadfully unhappy. Or you can think about that as a problem with their psychological adjustment, the way that they're looking at the world. If you look at the epidemiological literature, for example, one of the things that you find is that very many people have a first depressive episode after something genuinely terrible has happened to them, right they've lost someone or they've become injured or, or they've become unemployed because unemployment is a terrible shock to people, it's not precisely self evident that you can consider someone who's unhappy and desperate, because they no longer have a job depressed. They're certainly sad and they're not doing very well. But the fact that they no longer have an income is actually something with dramatic practical consequences. And treating that as if it's a mental disorder seems to be counterproductive. 

28:39  
It's also the case for example, that if someone comes into you to talk to you, and they're very upset and they may manifest the signs of an anxiety disorder or depression or or other other clinical features for that matter. You have to do a careful analysis of the manner in which they're embedded in their family because, this is something that will talk about quite thoroughly when we come to discussing Freud is that,  it's not like everybody's families are necessarily particularly happy places to be, I mean, human beings are very dependent, we have a very long period of dependency, partly because we're so critically hyper-developed, it takes a very long time to program us into something that's vaguely capable of maneuvering on its own. And that produces, of course, the very tight familial bonds that we all that we all desperately require, because who wants to be alone in the world, but it also, it also exposes us to the probability of becoming entangled into even multi-generational family pathology. And it isn't always obvious, and certainly hasn't been to me when I've seen my clients, that the fundamental problem with the client is the client. Sometimes the fundamental problem is the family.

30:03  
and perhaps that person has been identified as the problem person, it's rather convenient for everyone who's involved to make that presupposition. It's also the case that this is the Freudian and idea fundamentally, that it's very easy for people to become over-dependent on their parents and for the parents to facilitate that. And then for the primary developmental problem for the individual, in fact, to become free of the interfering elements of the family so that they can exist as independent individuals well. And then of course, there are cultural variations in that that make that proposition complex, but that's a fundamental tenet, say, of Freudian psychology.

30:55  
The clinical psychologists, all of whom that we're going to study, have a pronounced Western orientation. One of the fundamental presuppositions is that part of the hallmark of positive psychological development is the creation of an individual that's capable of acting independently. And that's, I would say, an implicit ideal that lurks at the bottom of the clinical presuppositions of old theorists that are classic psychologists.

#### Journey to the Underworld

31:31  
That's a very old picture. It's Jonah emerging from the whale. It's a variant of a myth.

31:40  
The myth is the dragon myth, I suppose. The dragon myth is that there's a dragon that lives under the ground that's eternal. Now and then it rises out of the ground to threaten the state and someone within the state determines to go confront the dragon voluntarily, and does so and then brings back something of great value. The hero being generally male, sometimes the thing of great value is a female that the Dragon has kidnapped. That's [St. George's story](https://en.wikipedia.org/wiki/Saint_George_and_the_Dragon). And sometimes it's gold, and other treasure like in the story of The Hobbit and story that you all know very well. 

32:30  
It's a classic [hero's story](https://en.wikipedia.org/wiki/Hero%27s_journey) and the hero's story is another fundamental element of the clinical theories, I would say. It's predicated on the idea that you learn through voluntary contact with what frightens or disgusts you. And that's a hallmark of psychoanalytic theory. Carl Jung, who we'll discuss in detail, said *In sterquiliniis invenitur*, which I'm sure I massacring Because it's Latin, but it meant "in filth, it will be found". And one of the hallmarks of the clinical theories is that within the confines of everyone's experience, and you can think about this, as experienced out in the world or experienced in the unconscious mind... There are

33:20  
dirty little secrets, let's say, and skeletons and dreadful old fears and remnants of abuse and memories of pathological behavior and failures of courage, that you leave you undeveloped, perhaps out of avoidance and that the psychoanalytic process is precisely the careful encounter with those forgotten and and repressed elements of the self in the hope that a clear encounter will redeem the unite them with the remainder of your personality and make your make you stronger in consequence. And I would say that that's just a variant of the manner in which human beings learn. And we'll talk about this more in relationship to PHA, because you always learn when you're wrong, which is very annoying. 

What do you learn when you're correct? You're walking in the world, you're operating in the world, you have a sense of what you want to have happen. You're always looking at the world through this sense of what you want to have happen. You're acting so that what you want to have happen, will happen. And when it happens, well, then you're happy because well, first of all, you get what you want. And that's good, maybe, depending on what you want, but it's also good because if you get what you want when you act, then it turns out that your model of how to act is valid, right? The outcome that you get what you want, indicates no error on part of your model. 

But it's very frequently the case that when you act to get what you want, you don't get what you want. And then that's unpleasant because you don't get what you want. But it's even more unpleasant because it brings with it the hint of a suggestion that the manner in which you're constructing the world is incorrect, at some indeterminate level. So for example, if you tell a joke, at a party, you presume that people will attend. And then when they hear the joke, they will laugh. And then if you tell the joke, and it goes flat, or even worse, disgusts and offends people, then you're going to be taken aback. And that's partly because you didn't get what you want. And that's not so good. But it's but it's more because there's something wrong with the way you conceptualize the situation. 

And then you're faced with a problem and the problem is that The emergence of a domain of the unknown. It's like, well, what kind of mistake did you make? Maybe you're not as funny as you think you are. That that can be a big problem. Maybe you're not around people that who are the way you think they are, maybe they don't like you as much as you thought they liked you. I mean, the, the potential for various paranoid thoughts of increasing severity, to come welling up at you, in a situation where you make even a trivial social mistake is quite broad. And when you make an error of that sort, you have to face it and sort through all the possibilities, so that you can find out what it was that you did wrong, and how to retool it so that in the future, you don't make the same mistake. And that requires, well, that requires in some sense, what you might describe as a journey into the belly of the beast, the beast being that place where things have fallen apart and where you're overwhelmed with negative emotion and chaos and concern. fusion. And that's a very old story. 

That's the story of the journey to the underworld. And the hero is the person who makes the voluntary journey to the underworld to collect what's been languishing down there. And that's the basic motif of psychoanalytic theory. I would say it's the basic motif in some sense of clinical practice because one of the things that you do as a clinician is find out what people are afraid of, and what they're avoiding, and that can be in their past or in their present, or in their future. Break it down into smaller pieces, and help them devise strategies of approach and mastery, and that improves the quality of their personality and helps develop them into people who won't make the same mistakes over and over again. 

38:00  
Alright, so well, why these this plethora of tools I said, in some sense, being a personality psychologist is like being an engineer. You're trying to build better people, when you might say that if you're a carpenter, or a mechanic, that your ability to fix a vehicle, or build a house is dependent on your proficiency with regards to the use of a multitude of tools. And so then you might say, well, the more tools you have at your disposal, the more likely it is that you're going to manage things properly. 

What I would like to offer you is the possibility that what you're going to encounter in this course, is a series of sophisticated conceptual tools that will help you understand yourself better, and therefore better orient yourself in the world. I regard this course is intensely practical. And that's because I believe that you have nothing to rely on, that's more crucial to your success, as you move through life, than your character and your personality. That's what you bring to every situation. And the more sophisticated you are in relationship to yourself and others, the more you understand people, the deeper you understand the nature of your own being, the more likely it is that you're going to proceed through your life in a manner that will make you pleased to exist rather than displeased to exist. 

I've collected the writings of people that I regard as of incomparable brilliance. They're difficult to understand. Their concepts are complex, but it's not surprising because the subject matter is complex and vital. And so it requires work and I would say try to keep up on the readings. It's going to make the chorus much more much richer for you. And I would say, because people often ask me, well, how should I read for this course? Because there's a lot of reading? And the answer is read as if it matters. That's the right answer. Don't be thinking about how it's going to be tested. If you do the readings and you come to the lectures, then the tests aren't particularly difficult. But you should read the readings as if the person is writing about you. And you should try to understand what the person says because it's another tool for you to use. 

With my clients, I use the approaches of one theorists for one client and the approaches of another theorist for another client, it seems to me to depend to some degree actually on the temperament of the client. I found for example, that people who are very high and openness which is the creativity dimension, are quite amenable to a young approach, whereas people who are more practical consciousness Low and openness are much more amenable to a behaviorist approach. We don't really know enough about psychology yet to match treatment to temperament, but those are the some of the things that I've experienced. 

#### Practicalities

**Okay practicalities?** Well, there's a website. The URL lists all the readings that aren't in the textbook. And so the textbook contains the classic readings, readings from people like Jung and Freud,  Piaget, Rogers, and so forth. And as I said, I picked that particular textbook because I believe that the author did a very credible job of summarizing what's very difficult to summarize. So and then also, on the website, there are links to papers, because much of the more modern material that pertain say to neuroscience and also to trade personality, I think it's better just to read the original papers, and I'll detail them out with you as we go through. And that'll also give you some familiarity with original psychological papers, which are again, there's a, there's an idiom that you have to master in order to understand them. But you might as well practice at especially if you're interested in continuing with psychology, in your educational practice or as a career. It's good to get accustomed to it. 

42:50  
Alright, so here's what we're going to cover.

Well, today, obviously, this is the introduction and overview classes a little strange this year because one day it's an hours, and the next day, it's two hours. So not exactly sure how we're going to negotiate our way through that. But we'll figure it out. historical perspectives, mythological representations. Well, I told you that I would try to provide you with a meta narrative that might enable you to link the theories that we're going to talk about together. So I'm going to describe to you what you might regard as a conceptual language. And as far as I can tell, it's the imagine, imagine that there are two kinds of things that you need to know. And I believe this to be the case, I believe that you need to know what the world is made of. And I suppose that's the proper domain of science. But then you need to know how to act. And that's a whole different thing. And you need to know how to act that's the thing you need to know most of anything anything because of course, you're a living creature and action, in relationship to desire goals is is everything to you and you can think about that from Darwinian perspective, you have to act at least so that you can survive at least so that you can find a partner. That's, that's life. And so part of the question is, well, how does the world look? If you think about it as a place to act? And the answer isn't a place of value free objects, that's not what the world looks like. And you can't act in a world of value free objects, because there's no way of choosing between. If everything has zero value, why would you choose one thing over another, you live in a world where things present themselves to you as a different value? And that's partly a consequence of your temperament, although it's a consequence of other things. 

What I'm going to try to do is to provide you with a schema that describes the world of morality, roughly speaking, which is how to act and tell you a little bit about what I think the language is, which I think was derived from Darwinian processes. And I believe that it's within that structure that the clinical theories logically nest. And so that'll give you a way of linking one theory to another from a conceptual perspective, without having to rely so much on sheer memorization. Then we'll talk about heroic and demonic initiations and the reason we're going to do that is because people, people use demonic initiations for 10s of thousands of years all over the world, and they have a particular kind of structure. This paper by [Mircea Eliada](https://www.jordanbpeterson.com/docs/230/2014/02Eliade.pdf), which is linked on the site is a very interesting one and details out some of these processes. 

There have been intelligent commentators like Henri F. Ellenberger, who wrote the [discovery of the unconscious](https://en.wikipedia.org/wiki/The_Discovery_of_the_Unconscious), which I think is an outstanding book, who linked the processes that the psychoanalysts uncovered in the late part of the 19th century and early part of the 20th century back to these more primordial rituals of personality transformation, and so We're going to situate ourselves in some sense in deep history, talking first about the underlying mythological landscape, then talking about archaic modes of personality conceptualization and transformation, and then moving from there into constructivism, and we're going to concentrate mostly on john Piaget, who is a developmental psychologist. constructivist believe that you make yourself out of the information that you gather in the world. So you're an exploring creature, you explore specifically when the maps that you're using in the world are no longer orienting yourself properly. When they're producing errors. You go out, gather information and assemble yourself from the information that you discovered. 

46:45  
Then the depth psychologists Jung and Freud, I think I'm going to with you, I'm going to walk you through
some films. I'm not going to use the films per se I'm going to use clips, stills from from the film, in chronological order. I'm going to try to explain to you how you might use Jungian presuppositions to understand what the films are about. if you think about a film, say like The Lion King, which is an extraordinarily popular film, it's a very strange phenomena, that you go and watch it. Right? I mean, think about it. It's drawings of animated animals that, in some sense, represent you. They're very low resolution, but you perceive them immediately, as living things. And you attribute to the motivation and, and, and motive power and understanding, you do it automatically without even thinking about it. And there's a classic plot that lies underneath those stories. And the plots are very, very, very, very old. And that's why you can understand them. And the reason you can understand them is because life has a plot, or maybe it has a couple of plots a multitude of plots, but Life has a plot and if it didn't, we wouldn't be able to understand each other. And so I'd like to illustrate that for you by analysis of some of these films. I think it's the best way to understand someone as sophisticated as Jung, who is very difficult to get a handle on. 

Freud. I'm going to do the same thing of the way I'm going to show you film with Freud. I'm going to show you a film called [crumb](https://www.youtube.com/watch?v=4FJiTCAmD4k), which is a documentary. And it's about a very badly enmeshed family and the attempts of the family members to I suppose escape that I'll talk to you about Freud, I'll show you the film that should give you a sense of Freudian psychopathology, which is a very difficult thing, otherwise to understand. Then there's a mid term and mid term is multiple choice. They'll do it in class. You'll you'll have lots of time to finish it. It'll cover the material that we took. We studied up to that point. Then we're going to talk about Rogers, Carl Rogers, who was a humanist. Rogers has a body centered philosophy, I suppose. He's interested as well in optimal personal development and the role that interpersonal communication plays in that Rogers hypothesis fundamentally, and it's a very interesting one is that honest communication between two people can produce personality transformation. And you might think, well, you kind of know that already, because there's something very engaging about a deep, honest conversation where you're able to say things that you wouldn't normally say, where you're being listened to by someone who's actually listening to you, and you're listening to them. And in the conversation, you're moving both of you further to a different point. That's different than a conversation where you're right and you're trying to convince me, or I'm right, and I'm trying to convince you, which I would say is the typical conversation. The healing conversation is more. Well, what's up with you, how are you doing? What, how's your life going? Where what sort of problems are you facing? What do you think about those problems? Can you conceptualize what a solution might be? Is there a way we could figure out how to up there, it's so it's a problem solving conversation. And it's predicated on the presupposition that the person that you're conversing with has the capacity to grow in a positive direction if they so choose. That's the fundamental. That's the fundamental presupposition of Rogerian psychology.

50:18  
May and Franco are also humanists.

52:00  
Who shouldn't take this course. Okay? First of all, if you didn't like this lecture, then don't take this course. Because this is what the lectures are going to be like. So and it's they're not for everybody. I use a lot of loose associations and try to gather them back in and I kind of wander around. That way I can talk directly to you, which I like doing, but I sacrifice a certain amount of organization for that. But my sense is that it's worth the sacrifice. The second thing is there's a lot of reading a lot and a lot of it is I would say a lot. It's hard science, the last half in particular, but the first half of lot of its philosophical in nature, philosophical, psychological. And so if you're not interested in that, like if you're a pure science type, and you're not interested in the clinical elements of personality, then in the investigating the philosophical underpinnings of those clinical theories, then I would say this is the course for you. And so you should take that seriously because the readings are hard. There's a lot of work involved in this course. And it would be better if you took a course that you actually wanted to take.

53:37  
Well, welcome to psychology 230 and we'll see you in a bit
