# Study Companion - PSY230h: Personality and its Transformations - Jordan B Peterson

[PSY230h: Personality and its Transformations - jordanbpeterson.com](https://www.jordanbpeterson.com/classes/personality-and-its-transformations/)

> The first half of the course deals with classic issues of psychology. The psychoanalytic thinkers — Freud, Jung and Adler — are placed in a context of pre-experimental religious and ritual thought, devoted to the transformation of habit and interpretive schema. The existentialists, phenomenologists, and humanists — concerned with the relationship between the individual, meaning and health — are discussed from the perspective of philosophy and political science. The constructivists are dealt with from a viewpoint that is simultaneously developmental and ontological.mapsofmeaning_traced
> 
> The second half of the course deals with issues of modern experimental psychology, from a more biological standpoint. Issues of motivation and information-processing are considered from within a standpoint that is essentially cybernetic: how do human beings operate in the world? What are their goals, their ends and means? What role do emotions and fundamental motivational states play in adaptation to the environment? How might personality traits and disorders be understood, from within such a framework?

## TOC

* [Course Playlists](#course-playlists)
* [Course Text](#text-selected-readings)
* [Syllabus](#syllabus)
* [Introduction and Overview](#introduction-and-overview)

### Course Playlists

To begin, I'll be transcribing the 2017 lectures, but maps of meaning is what I'm really into.

* [2014 Personality and Its Transformations (University of Toronto)](https://www.youtube.com/playlist?list=PL22J3VaeABQCfQy9Yg2y8fi5cI8HYUUct)
* [2015 Personality and Its Transformations (University of Toronto)](https://www.youtube.com/playlist?list=PL22J3VaeABQAhrMCQUa6sde_Y9DVbLYRv)
* [2016 Personality and Its Transformations (University of Toronto)](https://www.youtube.com/playlist?list=PL22J3VaeABQAOhH1CLMNnMl2R-O1abW1T)
* [2017 Personality and Its Transformations (University of Toronto)](https://www.youtube.com/playlist?list=PL22J3VaeABQApSdW8X71Ihe34eKN6XhCi) - 20 videos


### Text (Selected Readings)

* [Personality and its Transformations: Selected Readings](https://www.amazon.com/Personality-its-Transformations-Selected-Readings/dp/0921034466) *Paperback – 1998*
Peterson, J.B. Selections from Rychlak, Joseph. (1981). Introduction to Personality and its Transformations, Nelson. 

> *This is the paperback version, specially made for the PSY230H class*, and not the older hardcover. If you have the older hardcover, it can be used as well.

### Syllabus

* [PSY230h: Personality and its Transformations](https://www.jordanbpeterson.com/classes/personality-and-its-transformations/)

> Psychology 230H is a course that concentrates to a large degree on philosophical and neuroscientific issues, related to personality. It is divided into five primary topics, following an introduction and overview. The first half of the course deals with classic, clinical issues of personality; the second, with biological and psychometric issues. Students who are interested in clinical psychology, moral development, functional neurobiology and psychometric theory should adapt well to the class. An intrinsic interest in philosophical issues is a necessity.

#### Introduction and Overview	

[2017 Personality 01: Introduction](https://youtu.be/kYYJlNbV1OM) (54:03)
  * [2014 Lecture 01: Introduction and Overview](https://youtu.be/_0xBOMWJkgM) (1:10:07)
  * [2015 Lecture 01: Introduction & Overview](https://youtu.be/ZKpqpBRVr8Y) (1:15:23)
  * [2016 Lecture 01: Introduction and Overview (Part 1)](https://youtu.be/UGLsnu5RLe8) (1:06:19)

#### Historical & Mythological Context

[2017 Personality 02/03: Historical & Mythological Context](https://youtu.be/HbAZ6cFxCeY) (2:18:33)
  * [2014 Lecture 02: Mythological Representations](https://youtu.be/Owgc63KhcL8) (1:20:13)
  * [2015 Lecture 02: Historical Perspectives - Mythological Representations](https://youtu.be/9fKZPRAPT1w) (1:10:40)
  * [2016 Lecture 02: Introduction and Overview (Part 2)](https://youtu.be/ajtnhtEg76k) (51:36)
  * [2016 Lecture 03: Mythological Elements of the Life Story -- and Initiation](https://youtu.be/PH67HpFD2Ew) (1:05:11)

Peterson, J.B. (2013). [Three forms of meaning and the management of complexity](https://www.jordanbpeterson.com/docs/230/BC122013PetersonJBThreeKindsofMeaningFinal.pdf). In In K. Markman, T. Proulx, & M. Linberg (Eds.). The Psychology of Meaning (pp. 17-48). Washington, DC: American Psychological Association.

Eliade, M. (1958). [Heroic and shamanic initiations](https://www.jordanbpeterson.com/docs/230/2014/02Eliade.pdf). In Author, Rites and Symbols of Initiation (pp. 81-102). New York: NY.

#### Heroic & Shamanic Initiations

[2017 Personality 04/05: Heroic and Shamanic Initiations](https://youtu.be/wLc_MC7NQek) (2:37:35)
  * [2014 Lecture 03: Heroic & Shamanic Initiations (Part 01)](https://youtu.be/iEZVWWk6qHg) (1:19:34)
  * [2014 Lecture 04: Heroic & Shamanic Initiations (Part 02)](https://youtu.be/F9393El2Z1I) (1:22:17)
  * [2015 Lecture 03: Historical Perspectives - Heroic & Shamanic Initiations I Mircea Eliade](https://youtu.be/t966lVrHEzo) (1:20:08)
  * [2015 Lecture 04: Heroic & Shamanic Initiations II: Mircea Eliade](https://youtu.be/UFAyBEKKIBE) (1:23:37)

Eliade, M. (1958). [Heroic and shamanic initiations](https://www.jordanbpeterson.com/docs/230/2014/02Eliade.pdf). In Author, Rites and Symbols of Initiation (pp. 81-102). New York: NY.

#### Constructivism:

[2017 Personality 06: Jean Piaget & Constructivism](https://youtu.be/BQ4VSRg4e8w) (1:45:26)
  * [2014 Lecture 05: Jean Piaget (Constructivism)](https://youtu.be/91jWsB7ZYHw) (1:20:11)
  * [2015 Lecture 05: Constructivism: Jean Piaget](https://youtu.be/ED_TfmwjsEw) (1:13:48)
  * [2016 Lecture 04: Piaget Constructivism](https://youtu.be/G3fWuMQ5K8I) (1:12:12)

Piaget, J. (1962). [Play, dreams and imitation in childhood](https://www.jordanbpeterson.com/docs/230/2014/03Piaget.pdf). In Piaget, J. (1962). Play, Dreams and Imitation (pp. 147-168). New York: Norton.

Agnew, N.M., and Brown, J.L. (1989). [Foundation for a model of knowing](https://www.jordanbpeterson.com/docs/230/2014/04Agnew.pdf). Canadian Psychology, 30, 152-183.

#### Depth Psychology

##### Jung

[2017 Personality 07: Carl Jung and the Lion King (Part 1)](https://youtu.be/3iLiKMUiyTI) (54:03)
[2017 Personality 08: Carl Jung and the Lion King (Part 2)](https://youtu.be/X6pbJTqv2hw) (1:44:25)
  * [2014 Lecture 06: Carl Jung (Part 1)](https://youtu.be/8r8ISkQ4exM) (1:14:58)
  * [2015 Lecture 06: Depth Psychology: Carl Jung (Part 01)](https://youtu.be/DC0faZiBcG0) (1:19:49)
  * [2015 Lecture 07: Depth Psychology: Carl Jung (Part 02)](https://youtu.be/CFHZyse4VGw) (1:13:48)

Textbook: Jung

[Jung Quotations](https://www.jordanbpeterson.com/docs/230/2014/05Jungquotations.pdf)

Film: The Lion King

Jung, C.G. (1977). [Selected writings](https://www.jordanbpeterson.com/docs/230/2014/06Jung.pdf). In Sahakian, W.S. (Ed.). Psychology of Personality (3rd ed). (pp. 49-85). Boston: Houghton.

#### Depth Psychology - Freud

[2017 Personality 09: Freud and the Dynamic Unconscious](https://youtu.be/YFWLwYyrMRE) (48:22)
  * [2014 Lecture 09: Sigmund Freud II (Depth Psychology)](https://youtu.be/16WF1jLLyik) 
  * [2015 Lecture 08: Depth Psychology: Sigmund Freud (Part 1)](https://youtu.be/9Zji6xMkOgo) (1:14:40)
  * [2015 Lecture 09: Depth Psychology: Sigmund Freud (Part 02)](https://youtu.be/A07DV3FXyPo) (1:16:07)

Textbook: Piaget and Kelly
Textbook: Freud

[Freud Quotations](https://www.jordanbpeterson.com/docs/230/2014/07Freudquotations.pdf)
[Freudian analysis of The Cat in the Hat](https://www.jordanbpeterson.com/docs/230/2014/freudcat.pdf)

Freud, S. (1977). [Selected writings](https://www.jordanbpeterson.com/docs/230/2014/08Freud.pdf). In Sahakian, W.S. (Ed.). Psychology of Personality (3rd ed). (pp. 1-48). Boston: Houghton.

#### Depth Psychology - Rogers
##### Humanism/Existentialism/Phenomenology

[2017 Personality 10: Humanism & Phenomenology: Carl Rogers](https://youtu.be/68tFnjkIZ1Q) (50:10)
[2017 Personality 11: Existentialism: Nietzsche Dostoevsky & Kierkegaard](https://youtu.be/4qZ3EsrKPsc) (1:34:31)
  * [2014 Lecture 10: Carl Rogers (Phenomenological Humanism)](https://youtu.be/NUfvht7aJPQ) (1:13:24)
  * [2015 Lecture 10: Humanism: Carl Rogers](https://youtu.be/V9Ql5V7-OQo) (1:19:04)
  * [2016 Lecture 07: Phenomenology and Carl Rogers](https://youtu.be/3uJkd54p9dY) (1:15:53)

Textbook: Rogers

Film: Rogerian Psychotherapy

[CARL ROGERS & GLORIA COUNSELLING - Part 1](https://www.youtube.com/watch?v=ZBkUqcqRChg) 
  * [Part 2](https://www.youtube.com/watch?v=m30jsZx_Ngs)   
  * [Part 3](https://www.youtube.com/watch?v=RX_Y3zUPzEo)   
  * [Part 4](https://www.youtube.com/watch?v=zHxl5NtcDow)     
  * [Part 5](https://www.youtube.com/watch?v=L19nXMvbS8E)   
  * [Complete Rogers for Download](https://www.jordanbpeterson.com/classes/personality-and-its-transformations/Carl_Rogers/Carl%20Rogers%20Complete%2002.avi) 
[Rousseau and Chimps](https://www.jordanbpeterson.com/docs/230/2014/Rousseau%20and%20Chimps.pdf)

#### Humanism/Existentialism/Phenomenology

[2017 Personality 12: Phenomenology: Heidegger, Binswanger, Boss](https://youtu.be/11oBFCNeTAs) (46:33)
* [2014 Lecture 11: Existentialism: Viktor Frankl](https://youtu.be/zooE5GE81TU) (1:12:02)
* [2014 Lecture 12: Binswanger & Boss (Phenomenology)](https://youtu.be/UzdpzuEkL74) (1:13:20)
* [2014 Lecture 13: Aleksandr Solzhenitsyn (Existentialism)](https://youtu.be/8u3aTURVEC8) (1:17:39)
* [2015 Lecture 12: Existentialism: Dostoevsky, Nietzsche, Kierkegaard](https://youtu.be/SsoVhKo4UvQ) (1:15:45)
* [2016 Lecture 08: Existentialism: Nietzsche, Dostoevsky and Social Hierarchy](https://youtu.be/WjpV9mja3Wc) (1:12:48)
* [2016 Lecture 09: Phenomenology: Heidegger, Binswanger, Boss](https://youtu.be/539UQF6eT6I) (1:18:27)

May, R. (1958). [The Origins and Significance of the Existential Movement in Psychology](https://www.jordanbpeterson.com/docs/230/2014/09May.pdf). In R. May, E. Angel, and H.F. Ellenberger (Eds.), Existence: A New Dimension in Psychiatry and Psychology(pp. 3-36). New York: Basic Books.

Frankl, V.E. (1977). [Selected writings](https://www.jordanbpeterson.com/docs/230/2014/10Frankl.pdf). In Sahakian, W.S. (Ed.). Psychology of Personality (3rd ed).(pp. 184-203). Boston: Houghton.

Textbook: Binswanger and Boss

Heidegger Quotes: TBA

#### "Humanism/Existentialism/Phenomenology The Gulag Archipelago"

[2017 Personality 13: Existentialism via Solzhenitsyn and the Gulag](https://youtu.be/w84uRYq0Uc8) (1:41:04)
  * [2015 Lecture 13: Existentialism: Nazi Germany and the USSR](https://youtu.be/XY7a1RXMbHI) (1:18:44)
  * [2015 Lecture 14: Existentialism: Solzhenitsyn / Intro to Biology & Psychometrics](https://youtu.be/wZnqLvLbLV0) (1:22:45)

Solzhenitsyn, A. (1974). [Part IV, Ch. 1-4: The Soul and Barbed Wire](https://www.jordanbpeterson.com/docs/230/2014/12Solzhenitsyn.pdf). In Solzhenitsyn, A. The Gulag Archipelago, II.(pp. 595-672.). New York: Harper and Row.

#### "Biology and Traits: Psychometric Measurement"

[2017 Personality 14: Introduction to Traits/Psychometrics/The Big 5](https://youtu.be/pCceO_D4AlY) (49:37)
* [2014 Lecture 14: Psychometrics (Biology and Traits)](https://youtu.be/Om0YPe8c66Y) (1:08:03)
* [2016 Lecture 10: The Psychobiology of Traits](https://youtu.be/f511uRzsHhQ) (1:19:20)
* [2016 Lecture 11: The Psychobiology of Traits, Continued](https://youtu.be/RNxlEQSvh_w) (54:58)


Goldberg, L. R. (1992).  [The development of markers for the big five factor structure](https://www.jordanbpeterson.com/docs/230/2014/14Goldberg.pdf).  Psychological Assessment, 4, 26-42.

DeYoung, C. G., Quilty, L.C. & Peterson, J.B. (2007).  [Between facets and domains](https://www.jordanbpeterson.com/docs/230/2014/15DeYoung.pdf): 10 aspects of the Big Five.  Journal of Personality and Social Psychology, 93, 880-896.

Hirsh, J.B., DeYoung, C.G. & Peterson, J.B. (2009). [Meta-traits of the Big Five differentially predict engagement and restraint of behavior](https://www.jordanbpeterson.com/docs/230/2014/16Hirsh.pdf). Journal of Personality, 77, 1-17.

Optional: Allport, G.W. & Odbert, H.S. (1936). [Trait-names: a psycho-lexical study](https://www.jordanbpeterson.com/docs/230/1936%20Allport%20and%20Oddbert.pdf). Psychological Monographs, 47, 1-178.

##### Biology and Traits: The Limbic System and Lower-Order Goals

[2017 Personality 15: Biology/Traits: The Limbic System](https://youtu.be/AqkFg1pvNDw) (1:18:58)
  * [2014 Lecture 15: Limbic System & Goals (Biology and Traits)](https://youtu.be/_Jh8w6IVFs8) (1:18:54)

Gray, J.A. (1995). [A model of the limbic system and basal ganglia: applications to anxiety and schizophrenia](https://www.jordanbpeterson.com/docs/230/2014/17Gray.pdf). In Gazzaniga, M.S. (Ed.). The Cognitive Neurosciences(pp. 1165-1176). Cambridge: MIT Press.

OPTIONAL READING: Swanson. L.W. (2000). [Cerebral hemisphere regulation of motivated behavior](https://www.jordanbpeterson.com/docs/230/2014/13Swanson.pdf). Brain Research, 886, 113-164.

##### Biology and Traits: Incentive Reward and Threat: Extraversion and Neuroticism

[2017 Personality 16: Biology/Traits: Incentive Reward/Neuroticism](https://youtu.be/ewU7Vb9ToXg) (1:13:38)
  * [2014 Lecture 16: Extraversion & Neuroticism (Biology & Traits)](https://youtu.be/dYTAv7eQ-vg) (1:14:49)

LeDoux, J.E. (1998). [The Emotional Brain. Chapter 6: A few degrees of separation](https://www.jordanbpeterson.com/docs/230/2014/18LeDoux.pdf). New York: Simon and Schuster.

##### Biology and Traits: Aggression, Empathy and Agreeableness

[2017 Personality 17: Biology and Traits: Agreeableness](https://youtu.be/G1eHJ9DdoEA) (49:26)
  * [2014 Lecture 17: Agreeableness and gender differences](https://youtu.be/yOJR-nEhNMk) (1:17:36)
  * [2015 Lecture 17: Agreeableness - Aggression & Empathy](https://youtu.be/UgRaLmCOwYU) (1:16:31)

Peterson, J.B. & Flanders, J. (2005). [Play and the regulation of aggression](https://www.jordanbpeterson.com/docs/230/2014/19Petersonplay.pdf). In Tremblay, R.E., Hartup, W.H. & Archer, J. (Eds.). Developmental origins of aggression. (pp. 133-157). New York: Guilford Press.

Peterson, J.B. & Shane, M. (2004). [The functional neuroanatomy and psychopharmacology of predatory and defensive aggression](https://www.jordanbpeterson.com/docs/230/2014/20Petersonaggression.pdf). In J. McCord (Ed.). Beyond Empiricism: Institutions and Intentions in the Study of Crime. (Advances in Criminological Theory, Vol. 13) (pp. 107-146). Piscataway, NJ: Transaction Books.

See also [CARE: an innate brain system](http://www.mybrainnotes.com/motherhood-maternal-care.html)

##### Biology and Traits: Openness: Creativity and Intelligence

[2017 Personality 18: Biology & Traits: Openness/Intelligence/Creativity I](https://youtu.be/D7Kn5p7TP_Y) (1:45:50)
[2017 Personality 19: Biology & Traits: Openness/Intelligence/Creativity II](https://youtu.be/fjtBDa4aSGM) (48:58)
  * [2015 Lecture 18: Openness - Creativity & Intelligence](https://youtu.be/P6rm0LrO9vU) (1:23:12)
  * [2016 Lecture 13: Openness and Intelligence](https://youtu.be/qRFxulvRC7I) (1:15:28)

Gottfredson, L. S. (1997). [Why g matters: the complexity of everyday life.](https://www.jordanbpeterson.com/docs/230/2014/21Gottfredson.pdf)
Intelligence, 24, 79-132.

##### Biology and traits: The Prefrontal Cortex & High Order Goals

* [2015 Lecture 15: Biology & Traits: Limbic System & Lower Order Goals](https://youtu.be/RdNJTP6tYMs) (1:20:34)

Carver. C.S. & Scheier, M.F. (2001). [Goals and behavior](https://www.jordanbpeterson.com/docs/230/2014/22Carver.pdf). In (authors)., On the Self-Regulation of Behavior (Chapter 6; pp. 63-83). New York: Cambridge University Press.

##### 24. Biology and Traits: Disgust, Industriousness and Conscientiousness

[2017 Personality 20: Biology & Traits: Orderliness/Disgust/Conscientiousness](https://youtu.be/MBWyBdUYPgk) (1:34:33)
  * [2014 Lecture 20: Conscientiousness (Biology & Traits)](https://youtu.be/qH9-xsuPiUk) (1:12:22)
  * [2015 Lecture 16: Conscientiousness I - Industriousness & Disgust](https://youtu.be/vt90JwDHh-Y) (1:12:22)
  * [2015 Lecture 20: Conscientiousness - Industriousness, Orderliness & Disgust](https://youtu.be/35e5i6FQuMw) (1:15:58)
  * [2016 Lecture 12: Conscientiousness: Industriousness and Orderliness](https://youtu.be/q15eTySnWxc) (1:15:46)

Horberg, E.J., Oveis, C., Keltner, D. & Cohen, A.B. (2009). [Disgust and the moralization of purity](https://www.jordanbpeterson.com/docs/230/2014/23Horberg.pdf). Journal of Personality and Social Psychology, 97, 963-976

Film: [Triumph of the Will](https://archive.org/details/TriumphOfTheWillgermanTriumphDesWillens)

#### Biology and Traits: Performance Prediction

[2017 Personality 21: Biology & Traits: Performance Prediction](https://youtu.be/Q7GKmznaqsQ) (1:28:31)
  * [2014 Lecture 21: Performance Prediction (Biology & Traits)](https://youtu.be/hzMWpfHNYf0) (1:13:46)
  * [2015 Lecture 21: Performance Prediction](https://youtu.be/5p5YEvi8CHQ) (1:05:50)

Peterson, J.B. (2011). [Basic psychometric issues surrounding performance prediction](https://www.jordanbpeterson.com/docs/230/2014/24Petersonpsychometric.pdf). Unpublished manuscript.

Soldz, S. & Vaillant. G.E. [The Big 5 personality traits and the life course: a 45 year longitudinal study](https://www.jordanbpeterson.com/docs/230/2014/25Soldz.pdf). Journal of Research in Personality, 33, 208-232.

#### Conclusion: Psychology and Beliefs

[2017 Personality 22: Conclusion: Psychology and Belief](https://youtu.be/J9j-bVDrGdI) (1:08:29)
  * [2014 Lecture 22: Psychology & Belief (Conclusion)](https://youtu.be/qalR6Vx3Bpw) (1:18:31)
  * [2015 Lecture 22: Conclusion - Psychology and Belief](https://youtu.be/0v3x0ev1URY) (1:10:18)

Peterson, J.B. (2009). [Neuropsychology of motivation for group aggression and mythology](https://www.jordanbpeterson.com/docs/230/2014/26Petersonmythology.pdf). In Kurtz, L. (Ed.). Encyclopedia of Violence, Peace and Conflict (Volume 2 of 3) (pp. 1329-1340). Oxford: Elsevier.

Peterson, J.B. (2006). [Peacemaking among higher-order primates](https://www.jordanbpeterson.com/docs/230/2014/27Petersonpeacemaking.pdf). In Fitzduff, M. & Stout, C.E. (Eds.). The Psychology of Resolving Global Conflicts: From War to Peace. In Volume III, Interventions (pp. xx-xx). New York: Praeger.





